/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import "./style.css";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { Form, Button, Input } from "antd";
import { useDispatch, useSelector } from "react-redux"
import { getToken, login } from "../../redux/reducers/user";

const FormSignin = (props) => {


    const { boolean, type, condisi } = props
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const { token, loading } = useSelector((state) => state.user.userLogin);

    const masuk = async (values) => {
        dispatch(login(values))
    }

    const [form] = Form.useForm();

    useEffect(() => {
        if (getToken() !== null) {
            form.resetFields();
            navigate('/', { replace: true })
        }
    }, [token])
    const gagalMasuk = (errorInfo) => {
        console.log('Failed', errorInfo)
    }

    return (
        <div className="form-content-right w-100">
            <Form className="form w-100" onFinish={masuk} layout="vertical" onFinishFailed={gagalMasuk} autoComplete='off' noValidate>
                <div className="left" >
                    <h5>{type}</h5>
                </div>
                <Form.Item
                    className="form-inputs"
                    label="Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: "Masukan Email"
                        }
                    ]}
                >
                    <Input
                        className="border-i" placeholder="Contoh: mohamadnurizki427@gmail.com"
                    />

                </Form.Item>

                <Form.Item
                    className="form-inputs"
                    name="password"
                    label="password"
                    rules={[
                        {
                            required: true,
                            message: "Masukan Password"
                        }
                    ]}
                >
                    <Input.Password className="border-i" placeholder="Input Password" />
                </Form.Item>

                <div className="form-inputs">
                    <Form.Item>
                        <Button className="form-input-btn"
                            loading={loading}
                            disabled={loading}
                            htmlType="submit">
                            {type}
                        </Button>
                    </Form.Item>
                </div>
                <div className="form-input-login a">{boolean} punya akun? <span style={{ cursor: "pointer" }} onClick={() => navigate("/register")}>{condisi} di sini</span></div>
            </Form>
        </div>
    )
};

export default FormSignin;
