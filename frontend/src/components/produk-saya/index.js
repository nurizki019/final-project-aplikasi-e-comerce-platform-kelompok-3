import React from "react";
import { Card, Row, Col } from "antd";
import "./style.css";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getProductsByUser } from "../../redux/reducers/products";
import { BsPlusLg } from "react-icons/bs";
import Skeleton from "../skeleton"
import Kosong from "../../components/kosong"
import { isEmpty } from "../../utils/empty"

export default function CardListProduct() {

    const dispatch = useDispatch();

    const { data: products } = useSelector((state) => state?.products?.productByUser);
    const { loading: isLoading } = useSelector((state) => state?.products?.productByUser);

    useEffect(() => {
        dispatch(getProductsByUser());
    }, [dispatch]);

    return (
        <>
            <div className="size-s">
                {isLoading && <Skeleton />}
                <Row>
                    <Col span={8} className="mb-4">
                        <Link to={`/tambah-produk/`}>
                            <Card
                                className="size-a border-k p-2 d-flex justify-content-center align-items-center">
                                <div className="d-flex flex-column justify-content-center align-items-center">
                                    < BsPlusLg className="size-i neutral" />
                                    <p className="add neutral">Tambah Product</p>
                                </div>
                            </Card>
                        </Link>
                    </Col>

                    {(isEmpty(products)) ?
                        <Kosong />
                        :
                        products.map((product, i) => {
                            console.log(typeof (product?.product_image));
                            const parsing = JSON.parse(product?.product_image);
                            return (

                                <Col key={i} span={8} className="mb-4">
                                    <Link key={i} to={`/seller-product/${product?.id}`}>
                                        <Card
                                            className="size border-c p-2"
                                            bodyStyle={{ padding: "0" }}
                                            style={{ objectFit: "cover", }}
                                            cover={
                                                <img
                                                    className="size border-g"
                                                    alt="example"
                                                    src={parsing[0]}
                                                />
                                            }
                                        >
                                            <div key={i} className="size-c">
                                                <p className="title line mt-2">{product?.product_name}</p>
                                                <p className="desc line neutral">{product?.product_description}</p>
                                                <p className="title">Rp.{product?.product_price}</p>
                                            </div>
                                        </Card>
                                    </Link>
                                </Col>
                            )
                        })
                    }
                </Row>
            </div >
        </>
    )
}

