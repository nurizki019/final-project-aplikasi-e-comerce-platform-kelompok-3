/* eslint-disable no-unused-expressions */
import React, { useState, useEffect } from 'react'
import { Modal, Card } from 'react-bootstrap';
import { useNavigate, Link, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getProductById } from "../../../redux/reducers/products";
import { createOrder, reset } from "../../../redux/reducers/orders";
import { Button, Row, Col } from 'antd';


const ModalWa = ({ isShow, handleClose, sendToParent, data, whatsapp }, props) => {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { id } = useParams()

  const { data: productId } = useSelector((state) => state?.products?.productById);

  useEffect(() => {
    dispatch(getProductById(id));
  }, [dispatch, id]);

  try {
    const produk = JSON.parse(productId?.product_image)
    return (
      <Modal
        {...props}
        size="sm"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        show={isShow} onHide={handleClose} >
        <Modal.Header closeButton />
        <Modal.Body>
          <span>Yeay kamu berhasil mendapat harga yang sesuai</span>
          <div style={{ fontSize: 14, height: 70, color: "#8A8A8A" }}>
            <label>Segera hubungi pembeli melalui
              <p>whatsapp untuk transaksi selanjutnya</p>
            </label>
          </div>
          <Card className="p-2" style={{ borderRadius: 16, background: "#EEEEEE" }}>
            <Row gutter={16} className="mb-4">
              <Col span={6}>
                <Card.Img style={{ height: 57, borderRadius: 12 }} src={productId?.user?.photo} />
              </Col>
              <Col span={18}>
                <div className='mt-2' style={{ fontSize: 14 }}>{data?.user?.user_name}</div>
                <div style={{ fontSize: 14 }}>{data?.user?.address}</div>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={6}>
                <Card.Img style={{ height: 57, borderRadius: 12 }} src={produk[0]} />
              </Col>
              <Col span={18}>
                <div className='mt-2' style={{ fontSize: 14 }}>{productId?.product_name}</div>
                <div style={{ fontSize: 14 }}><s>{productId?.product_price}</s></div>
                <div style={{ fontSize: 14 }}>{data?.bid}</div>
              </Col>
            </Row>
          </Card>
        </Modal.Body>

        <Modal.Footer>
          <Button
            href={`https://api.whatsapp.com/send/?phone=${data?.product?.user?.phone}&text&type=phone_number&app_absent=0`}
            tyle={{ fontSize: 14, height: 50, width: 600, borderRadius: 16, background: "#7126B5", color: 'white' }} >
            Hubungi via Whatsapp
          </Button>
        </Modal.Footer>
      </Modal>
    );
  } catch (error) {
  }
}

export default ModalWa
