/* eslint-disable no-unused-expressions */
import React, { useState, useEffect } from 'react'
import { Card, Col, Modal, Row } from 'react-bootstrap';
import { useNavigate, Link, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getProductById } from "../../../redux/reducers/products";
import { createOrder, reset } from "../../../redux/reducers/orders";
import { Button, Form, Input } from 'antd';
import io from 'socket.io-client';
import jwt_decode from "jwt-decode";
import { getToken } from '../../../redux/reducers/user';

const socket = io.connect("http://localhost:8000");

const decoded = jwt_decode(getToken());

const BuyerModal = ({ isShow, handleClose, sendToParent }) => {

  const [seller, setSeller] = useState("");
  const [buyer, setBuyer] = useState("");
  const [product, setProduct] = useState("");
  const [message, setMessage] = useState("");
  const [price, setPrice] = useState("");
  const [messageReceived, setMessageReceived] = useState("");
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams()


  const { data: productId } = useSelector((state) => state?.products?.productById);
  const { data: order, loading: isLoading, succes: successOrder } = useSelector((state) => state.orders.postOrder);

  console.log(isLoading)
  console.log(order)


  const create = async (values) => {
    dispatch(createOrder({ data: values, id: productId?.id }))

    console.log(values)
  }
  const [form] = Form?.useForm();



  const createFailed = (errorInfo) => {
    console.log('Failed', errorInfo)
  }

  const [show, setShow] = useState(isShow)

  // kirim notifikasi
  const sendNotif = () => {
    if(seller  !== ""){
        socket.emit("send_offer", seller)
    }
    socket.emit("send_message", {
        message, 
        seller,
        product,
        price,
        status:true, 
        buyer});
  };

  useEffect(() => {
    setSeller(productId?.user_id);
    setProduct(productId?.product_name);
    setPrice(productId?.product_price);
    setBuyer(decoded.user_name);
    dispatch(getProductById(id));
    form.resetFields();
    if (successOrder) {
      sendToParent(true)
      dispatch(reset())
      return setShow(false)
    }
  }, [dispatch, form, id, successOrder, socket]);

  console.log("succes", successOrder)
  console.log(message)
  console.log(productId?.user_id)
  console.log(productId?.product_name)
  try {

    const parsing = JSON.parse(productId?.product_image)
    return (



      <Modal show={show} onHide={handleClose} >
        {/* {(isLoading === false) && navigate(`/seller-product/${id}`)} */}
        <Modal.Header closeButton >
          <Modal.Title>Masukan Harga Tawarmu</Modal.Title>
        </Modal.Header>
        <Form onFinish={create} onFinishFailed={createFailed} autoComplete='off' noValidate>
          <Modal.Body>
            <div style={{ fontSize: 14, height: 70, color: "#8A8A8A" }}>
              <label>Harga tawaranmu akan diketahui penjual,
                <p>jika penjual cocok kamu akan segera dihubungi penjual.</p>
              </label>
            </div>
            <Card className="p-2" style={{ borderRadius: 16, background: "#EEEEEE" }}>
              <Row>
                <Col className="col-2">
                  <Card.Img style={{ height: 57, borderRadius: 12 }} src={parsing[0]} />
                </Col>
                <Col>
                  <div className='mt-2' style={{ fontSize: 14 }}>{productId?.product_name}</div>
                  <div style={{ fontSize: 14 }}>{productId?.product_price}</div>
                </Col>
              </Row>
            </Card>
            <div style={{ fontSize: 12, borderRadius: 13 }}>
              <Row>
                <Col>
                  <div className="form-inputs-harga">
                    <label className=" mt-3">Harga Tawar</label>
                    <div className='mt-2'>
                      <Form.Item name="bid"
                        rules={[
                          {
                            required: true,
                            message: "Masukan Nego"
                          }
                        ]}>
                        <Input onChange={(event)=>{setMessage(event.target.value);}}style={{ borderRadius: 16 }}
                          className="form-input"
                          placeholder='Rp 0,00'
                        />

                      </Form.Item>
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </Modal.Body>
          <Modal.Footer>

            <Button onClick={sendNotif} loading={isLoading}
              disabled={isLoading}
              htmlType="submit"
              style={{ fontSize: 14, height: 50, width: 600, borderRadius: 16, background: "#7126B5", color: 'white' }} >
              Kirim
            </Button>

          </Modal.Footer>
        </Form>
      </Modal>


    );
  } catch (error) {

  }
}

export default BuyerModal
