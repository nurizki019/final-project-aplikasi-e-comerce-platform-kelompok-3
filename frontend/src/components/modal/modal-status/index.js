/* eslint-disable no-unused-expressions */
import React, { useState, useEffect } from 'react'
import { Modal, Card } from 'react-bootstrap';
import { useNavigate, Link, useParams, Location } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { editOrder } from '../../../redux/reducers/orders';
import { Button, Row, Col, Radio } from 'antd';
import { toast } from 'react-toastify';


const ModalStatus = ({ sShow, handleClose, sendToParent, data }, props) => {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [value, setValue] = useState();

  const onChange = (e) => {
    console.log('radio checked', e.target.value);
    setValue(e.target.value);
  };


  const { id } = useParams()

  const { loading: isLoading, succes: succesUpdate } = useSelector((state) => state.orders.putOrder);

  useEffect(() => {
    if (succesUpdate) {

      window.location.reload(false);
    }
  })


  const handleUpdate = () => {
    dispatch(editOrder({ id: id, data: value }));

  }

  console.log("status", value)
  return (
    <Modal
      {...props}
      size="sm"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={sShow} onHide={handleClose} >
      <Modal.Header closeButton />
      <Modal.Body>
        <span>Perbarui status penjualan produkmu</span>

        <Card className="p-2" style={{ borderRadius: 16, background: "#EEEEEE" }}>
          <Radio.Group onChange={onChange} value={value}>

            <Radio value={4}>Berhasil terjual</Radio>
            <div style={{ fontSize: 14, height: 70, color: "#8A8A8A" }}>
              <p>Kamu telah sepakat menjual produk ini kepada pembeli</p>
            </div>
            <Radio value={3}>Batalkan transaksi   </Radio>
            <div style={{ fontSize: 14, height: 70, color: "#8A8A8A" }}>
              <p>Kamu membatalkan transaksi produk ini dengan pembeli</p>
            </div>
          </Radio.Group>

        </Card>
      </Modal.Body>
      <Button
        // loading={isLoading}
        // disabled={isLoading}
        onClick={() => handleUpdate()}
        tyle={{ fontSize: 14, height: 50, width: 600, borderRadius: 16, background: "#7126B5", color: 'white' }} >
        Kirim
      </Button>
      <Modal.Footer>
      </Modal.Footer>
    </Modal>
  );
}

export default ModalStatus
