import { Bars } from "react-loader-spinner"
import "./style.css"

const LoadingBar = () => {
    <div className="btn-jual">
        <Bars className="fixed-top" color="#00BFFF" height={80} width={80} />
    </div>
}
export default LoadingBar