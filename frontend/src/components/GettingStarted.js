import React from "react";
import { useEffect, useState } from "react";
import { Col, Row, Container } from "react-bootstrap";
import { FaSearch } from "react-icons/fa";
import { TbPlus } from "react-icons/tb";
import { Carousel } from 'react-responsive-carousel';
// import 'react-responsive-carousel/lib/styles/carousel.min.css';
import OwlCarousel from "react-owl-carousel";
import axios from 'axios';
import { useNavigate } from "react-router-dom";

const GettingStarted = () => {
  // 1 untuk memanggil data dari internet dengan cara memanggil useState (setProduct)
  const [products, setProducts] = useState([]);

  const navigate = useNavigate();

  // 2 makai useEffect untuk integrasi
  useEffect(() => {
    axios.get('https://back-end-seken.herokuapp.com/products')
      .then(res => res.data)
      .then(res => {
        // 3 kemudian res=respone-kemudian memanggil data-di dalam data ada content
        // dan dimasukan kedalam state products
        setProducts(res.data.content)
      })
  }, []);

  return (
    <div className="mt-5">
      {/* <Carousel
      autoPlay
      centerMode
      centerSlidePercentage={65}
      emulateTouch
      infiniteLoop
      selectedItem={1}
      showArrows={false}
      showStatus={false}
      showThumbs={false}
      showIndicators={false}
      className="mt-5 pt-4 pb-4"
    >
      {data.map(({ img }) => (
        <div key={uuid()} className={[styles.box]}>
          <img className="px-2" src={img} alt={img} layout="fill" />
        </div>
      ))}
    </Carousel> */}

      <section className="testimonial mb-4" id="testimonial">
        <OwlCarousel
          autoplay
          centerMode
          emulateTouch
          infiniteLoop
          selectedItem={1}
          showArrows={false}
          showStatus={false}
          showThumbs={false}
          showIndicators={false}
          dots={false}
          items={2}
          className="owl-theme"
          loop
          // nav
          center
          margin={10}
        >
          <Row className="mt-4">
            <Col>
              <div className="item-ramadhan">
                <div className="testi-body">
                  <div className="card-right">
                    <h1 className="card-title">Bulan Ramadhan</h1>
                    <h1 className="card-title">Bulannya Diskon!</h1>
                    <h6 className="card-title">Diskon Hingga</h6>
                    <p className="diskon">70%</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
          <Row className="mt-4">
            <Col>
              <div className="item-merdeka">
                <div className="testi-body">
                  <div className="card-left">
                    <h1 className="card-title">Bayar Setengah</h1>
                    <h1 className="card-title">di Hari Merdeka!</h1>
                    <p className="card-title">Diskon Hingga</p>
                    <p className="diskon">50%</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
          <Row className="mt-4">
            <Col>
              <div className="item-newyear">
                <div className="testi-body">
                  <div className="card-left">
                    <h1 className="card-title text-white">Tahun Baru</h1>
                    <h1 className="card-title">Barang Baru Ga Si'</h1>
                    <h6 className="card-title">Diskon Hingga</h6>
                    <p className="diskon">35%</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </OwlCarousel>
      </section>

      <Container>
        <div className="search-title">
          <h1>Telusuri Kategori</h1>{" "}
        </div>{" "}
        <div className="btn-category">
          {" "}
          <div className="">
            {" "}
            <button className="btn-list-cat active">
              <FaSearch /> Semua{" "}
            </button>{" "}
            <button className="btn-list-cat">
              <FaSearch /> Hobi{" "}
            </button>{" "}
            <button className="btn-list-cat">
              <FaSearch /> Kendaraan{" "}
            </button>{" "}
            <button className="btn-list-cat">
              <FaSearch /> Baju{" "}
            </button>{" "}
            <button className="btn-list-cat">
              <FaSearch /> Elektronik{" "}
            </button>{" "}
            <button className="btn-list-cat">
              <FaSearch /> Kesehatan{" "}
            </button>{" "}
          </div>{" "}
        </div>
        <div className="d-flex">
          <Row>
            {/* 4 untuk memanggil state di looping item nya dengan menggunakan .map */}
            {products.map(product => {
              // 5 metode ini digunakan untuk mem-parsing JSON menjadi object JavaScript
              const parsing = JSON.parse(product.product_image);

              const productId = product.id;
              console.log(productId);
              return (
                // 6 ketika di klik dan untuk menavigasi ke halaman buyer 
                // dengan menentukan productId kedalam query
                <Col onClick={() => navigate(`/buyer?productId=${productId}`)} className="d-flex justify-content-start mt-4">
                  <div className="card card-all">
                    <div className="card-body">
                      <img src={parsing[0]} alt="" />
                      <h4 className="text-merk text-dark mt-2">{product.product_name}</h4>
                      <p className="card-description text-muted">{product.product_description}</p>
                      <h5 className="text-merk text-dark mt-2">{product.product_price}</h5>
                    </div>
                  </div>
                </Col>
              )
            })}
          </Row>
        </div>
        <div className="text-center button-jual">
          <div className="">
            <button onClick={() => navigate("/infoproduk")} className="btn-jual fixed-bottom">
              <TbPlus /> Jual
            </button>
          </div>
        </div>
      </Container>
    </div>


  );
};

export default GettingStarted;
