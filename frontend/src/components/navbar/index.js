import React from "react";
import { Nav, Navbar, Container, Card, Col, Row } from "react-bootstrap";
import { FaSearch, FaUser, FaList, FaBell } from "react-icons/fa";
import { FiLogIn } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import SellerNotifikasi from "../notifikasi/index";
import { useEffect, useState } from "react";
import io from 'socket.io-client';
import { Outlet } from "react-router-dom";
import Akun from "../akun"
import "./style.css"
import jwt_decode from "jwt-decode";
import { getToken } from '../../redux/reducers/user';


const socket = io.connect("http://localhost:8000");
// import Vector from "../assets/images/Vector.png";
// import User from "../assets/images/User.png"

const notifPopupStyle = {
  width: 360,
  position: 'absolute',
  right: 85,
  top: 56,
  zIndex: 999,
  backgroundColor: '#fff',
  padding: 10,
  borderRadius: 13,
  margin: 5,
  boxShadow: '5px 5px 15px 0px rgba(0,0,0,0.1)'
};

const decoded = jwt_decode(getToken());

const NavigationBar = () => {
  const navbar = useNavigate();
  const [seller, setSeller] = useState("")
  const [messageReceived, setMessageReceived] = useState("");
  const [idUserReceived, setIdUserReceived] = useState("");
  const [popup, setPopup] = useState(false);
  const [popUpAkun, setPopUpAkun] = useState(false);

  const AkunPopUp = () => (
    <Card style={notifPopupStyle}>
      <Akun />
    </Card>
  );
  const popUpAkunHandler = (state) => {
    setPopUpAkun(!state)
    setPopup(false)
  }

  const [isLogedIn, setIsLogedIn] = useState(false);
  const [isNotif, setIsNotif] = useState(false);



  useEffect(() => {
    
    const token = localStorage.getItem('token');
    if (token) {
      setIsLogedIn(true);
      // setSeller(decoded.id)
      socket.on("receive_message", (data) => {
        setMessageReceived([data.message, data.product, data.buyer, data.price, data.status]);
      })
    }
  }, [socket]);
  try{
    socket.emit("send_offer", seller)

    setIsNotif(messageReceived[4])
    console.log(decoded.id);
    console.log(messageReceived[4]);
    console.log(decoded.user_name);
    console.log({idUserReceived});
    console.log(messageReceived); 
    // console.log(status);
  }
  catch (err){
    console.log(err);
  }
  // const message = messageReceived;
  const NotifPopUp = () => (
    <Card style={notifPopupStyle}>
      <SellerNotifikasi erNotifikasi 
      offer = {messageReceived[0]}
      product = {messageReceived[1]}
      price = {messageReceived[3]}
      />
    </Card>
  );

  const popupHandler = (state) => {
    setPopup(!state)
    setPopUpAkun(false)
    // if(seller  !== ""){
    //   socket.emit("send_offer", seller)
    // }
  }
  

  return (
    <>
      <Navbar className="shadow navs fixed-top" bg="light" variant="light" >
        <Container>
          <Navbar.Brand onClick={() => navbar("/")} className="text-brand" style={{ color: "#7126B5", cursor: 'pointer' }}>Second Hand</Navbar.Brand>
          <div className="search-bar me-auto">
            <div className="search-box">
              <input className="search-txt" type={"text"} placeholder={"Cari di sini..."} />
              <a className="search-btn" href=" ">
                <FaSearch />
              </a>
            </div>
          </div>
          <Nav>
            <div className="text-center">
              <div className="">
                {isLogedIn ?
                  <>
                    <Row>
                      <Col style={{ cursor: 'pointer' }} onClick={() => navbar('/myProduct')}><FaList color="#151515" /></Col>
                      <Col style={{ cursor: 'pointer' }} onClick={() => popupHandler(popup)}><FaBell color="#151515" /></Col>
                      <Col style={{ cursor: 'pointer' }} onClick={() => popUpAkunHandler(popUpAkun)}><FaUser color="#151515" /></Col>
                    </Row>
                  </> :
                  <button onClick={() => navbar("/login")} className="btn-masuk">
                    <FiLogIn />{" "}
                    Masuk
                  </button>}
              </div>
            </div>{" "}
          </Nav>
        </Container>
        {popup &&
          <NotifPopUp />}
        {popUpAkun &&
          <AkunPopUp />}
      </Navbar>

      <Outlet />
    </>
  );
};

export default NavigationBar;
