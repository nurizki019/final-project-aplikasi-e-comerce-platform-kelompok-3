/* eslint-disable jsx-a11y/img-redundant-alt */
import React from "react";
import { Card, Row, Col } from "antd";
import "./style.css";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getOrderWish } from "../../redux/reducers/orders";
import Skeleton from "../skeleton"
import Kosong from "../../components/kosong"
import { isEmpty } from "../../utils/empty"

export default function CardListOrdersM() {

    const dispatch = useDispatch();
    const { data: orders } = useSelector((state) => state?.orders?.wishlist);
    const { loading: isLoading } = useSelector((state) => state?.orders?.wishlist);

    console.log(orders)

    useEffect(() => {
        dispatch(getOrderWish())
    }, [dispatch]);
    try {

        return (

            <div className="size-s">
                {isLoading && <Skeleton />}
                <Row gutter={20} >

                    {(isEmpty(orders)) ?
                        <Kosong type='diminati' />
                        :
                        orders.map((order, i) => {

                            const parsing = JSON.parse(order.product.product_image);
                            return (
                                <Col key={i} span={8} className="mb-4">
                                    <Link key={i} to={`/diminati/${order?.status_id}/${order?.id}/${order?.status?.status_name}`}>
                                        <Card key={i}
                                            className="size border-c p-2"
                                            bodyStyle={{ padding: "0" }}
                                            style={{ objectFit: "cover", }}
                                            cover={
                                                <img
                                                    className="size border-g"
                                                    alt="masi di coba"
                                                    src={parsing[0]}
                                                />
                                            }
                                        >
                                            <div key={i} className="size-c">
                                                <p className="title line mt-2">{order.product.product_name}</p>
                                                <p className="desc line neutral">{order.product.product_description}</p>
                                                <p className="title">Rp.{order.product.product_price}</p>
                                            </div>
                                        </Card>
                                    </Link>
                                </Col>
                            )
                        })
                    };
                </Row>
            </div>
        )
    } catch (error) {

    }

}

