import React from "react";
import { Navbar, Container, Form } from "react-bootstrap";
import { FaChevronLeft } from "react-icons/fa";
import { FiPlus } from "react-icons/fi";
import { useNavigate } from "react-router-dom";

export default function InfoProduk() {
  const infoproduk = useNavigate();
  return (
    <div>
      <Navbar className="navs fixed-top shadow" variant="light" bg="light">
        <Container>
          <div className="back-mobile">
              <button onClick={() => infoproduk("/")} className="btn-back" href="/">
                <FaChevronLeft />
              </button>
            </div>
          <Navbar.Brand className="text-brand" style={{color:"#7126B5"}}>SecondHand</Navbar.Brand>
          <div className="text-info-produk text-black">Lengkapi Detail Produk</div>
        </Container>
      </Navbar>

      <div className="body-produk">
        <Container>
          <Form className="form-produk box-form">
            <div className="">
              <button onClick={() => infoproduk("/")} className="btn-back" href="/">
                <FaChevronLeft />
              </button>
            </div>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Nama Produk</Form.Label>
              <Form.Control type="text" placeholder="Nama Produk" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Harga Produk</Form.Label>
              <Form.Control type="Number" placeholder="Rp 0,00" />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Kategori</Form.Label>
              <Form.Select>
                <option className="text-muted" label="Pilih Kategori" />
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </Form.Select>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Deskripsi</Form.Label>
              <Form.Control as="textarea" placeholder="Deskripsi" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Foto Produk</Form.Label>
              <div>
                <label for="first-img">
                  <i className="fa add-img"><FiPlus /></i>
                </label>
                <input type="file" className="input-file" id="first-img" />
              </div>
            </Form.Group>

            <button className="btn-preview" variant="primary" type="submit">
              Preview
            </button>
            <button className="btn-terbitkan" variant="primary" type="submit">
              Terbitkan
            </button>
          </Form>
        </Container>
      </div>
    </div>
  );
}
