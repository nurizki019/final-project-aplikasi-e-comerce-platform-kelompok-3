import React from 'react'
import { Card, Col, Row } from 'react-bootstrap'
import Penjual from "../../assets/images/Penjual.png";

const SellerNotifikasi = (props) => {
  const {offer, product, price} = props;
  return (
    <div className="mt-2 p-2" style={{ borderRadius: 13 }}>
      <Row>
        <Col className="col-2">
          <Card.Img style={{ width: 50 }} src={Penjual} />
        </Col>
        <Col>

          <Row>
            <Col>
              <small style={{ fontSize: 10 }}>Penawaran produk
              </small>
            </Col>
            <Col>
              <small style={{ fontSize: 10, right: 0 }}>20 Apr, 14.00</small>
            </Col>
          </Row>
          <div style={{ fontSize: 14 }}>{product}</div>
          <div style={{ fontSize: 14 }}>Rp. {price}</div>
          <div style={{ fontSize: 14 }}>Ditawar Rp. {offer}</div>
        </Col>
      </Row>
    </div>
  )
}
export default SellerNotifikasi;