/* eslint-disable react/jsx-no-comment-textnodes */
import React, { useEffect, useState } from "react";
import { Row, Col, Card } from "react-bootstrap";
import { Button } from "antd"
import "./style.css"
import { useLocation, Link, useNavigate, useParams } from "react-router-dom";
import BuyerModal from "../modal/buyer-modal";
import orders from "../../redux/reducers/orders";

const CarouselD = (props) => {

  const { data } = props;

  const [showModal, setShowModal] = useState(false);

  const { id } = useParams()

  const onBtnClickHandler = () => { setShowModal(true) }

  const [isParentData, setIsParentData] = useState(false);

  const location = useLocation();


  const [seller, setSeller] = useState(false);
  const [buyer, setBuyer] = useState(false);

  console.log("seller", seller)
  console.log("buyer", buyer)
  console.log(location.pathname)
  useEffect(() => {
    if (location?.pathname === `/seller-product/${id}`) {
      return setSeller(true);
    }
    else if (location?.pathname === `/buyer-product/${id}`) {

      return setBuyer(true);
    }
  }, [location?.pathname]);

  return (
    <>
      <Col md={6}>
        <Card style={{ borderRadius: 13 }}>
          <Card.Body>
            <Card.Title className="text-black" style={{ fontSize: 16 }}>
              <span>{data?.product_name}</span>
            </Card.Title>

            <Card.Text className="aksesories mb-2 mb-md-3" style={{ fontSize: 14 }}>
              <span>{data?.category?.category_name}</span>
            </Card.Text>

            <Card.Text className="harga mb-0 mb-md-2" style={{ fontSize: 14 }}>
              <span>{data?.product_price}</span>
            </Card.Text>

            <div className="form-inputs">
              {buyer &&
                <Button onClick={onBtnClickHandler}
                  className="seller-buyer"
                  disabled={isParentData}
                  type="submit">
                  Saya tertarik dan ingin nego
                </Button>
              }
              {seller &&
                <>
                  <Button className="seller-buyer" type="submit">
                    Terbitkan
                  </Button>
                  <Button className="seller-buyer" type="submit">
                    <Link to={`/edit-produk/${data?.id}`}>
                      Edit
                    </Link>
                  </Button>
                </>

              }
            </div>

            {/* ----- MODal------- */}
            {showModal &&
              <BuyerModal toChild={isParentData} sendToParent={setIsParentData} isShow={showModal} handleClose={() => setShowModal(false)} data={data?.product_name} dat={data?.product_price} />
            }


          </Card.Body>
        </Card>
        <Card className="mt-2 p-2" style={{ borderRadius: 13 }}>
          <Row>
            <Col className="col-2">
              <Card.Img style={{ width: 50 }} src={data?.user?.photo} />
            </Col>
            <Col>
              <div className="nama-penjual" style={{ fontSize: 14 }}>
                <span>{data?.user?.user_name}</span>
              </div>
              <div className="kota">
                <small style={{ fontSize: 10 }}>{data?.user?.address}</small>
              </div>
            </Col>
          </Row>
        </Card>
      </Col>
    </>

  )
}

export default CarouselD;