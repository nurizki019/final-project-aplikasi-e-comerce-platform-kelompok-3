import React, { useState } from "react";
import { Container, Row, Col, Alert} from "react-bootstrap";
import {
  FaPlus,
  FaHeart,
  FaDollarSign,
  FaCube,
  FaAngleRight,
  FaSearch,
} from "react-icons/fa";
// import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { Card } from "react-bootstrap";
import NavigationBar from "./NavigationBar";
import nike1 from "../assets/images/shoes/1.jpg";
import nike2 from "../assets/images/shoes/2.jpg";
import nike3 from "../assets/images/shoes/3.jpg";
import { Link, useNavigate } from "react-router-dom";

import "../style/SellerDaftarJual.css";

const SellerDaftarJual = () => {
  const [show, setShow] = useState(true);

  const navigate = useNavigate();
  const dummyData = [
    {
      name: "Nike A",
      description: "asdasdasd",
      price: "Rp. 100.000",
      image: nike1,
    },
    {
      name: "Nike B",
      description: "asdasdasd",
      price: "Rp. 100.000",
      image: nike2,
    },
    {
      name: "Nike C",
      description: "asdasdasd",
      price: "Rp. 100.000",
      image: nike3,
    },
    {
      name: "Nike A",
      description: "asdasdasd",
      price: "Rp. 100.000",
      image: nike1,
    },
    {
      name: "Nike B",
      description: "asdasdasd",
      price: "Rp. 100.000",
      image: nike2,
    },
  ];

  const cardListProduct = (product) => {
    if (product) {
      return (
        <Col>
          <Card
            className="card-add__produk h-100"
            onClick={() => navigate("/seller")}
          >
            <Card.Img
              variant="top"
              src={product.image}
              style={{
                width: "100%",
                height: "30vh",
                objectFit: "cover",
              }}
            />
            <Card.Body>
              <Card.Title
                style={{
                  color: "black",
                }}
              >
                {product.name}
              </Card.Title>
              <Card.Text>{product.description}</Card.Text>
              <Card.Title
                style={{
                  color: "black",
                  fontSize: "1rem",
                }}
              >
                {product.price}
              </Card.Title>
            </Card.Body>
          </Card>
        </Col>
      );
    } else {
      return (
        <Col>
          <Card
            className="card-add__produk h-100"
            onClick={() => navigate("/infoproduk")}
          >
            <Card.Body className="d-flex align-items-center justify-content-center">
              <p
                style={{
                  textAlign: "center",
                }}
              >
                <FaPlus /> Tambah Produk
              </p>
            </Card.Body>
          </Card>
        </Col>
      );
    }
  };

  return (
    <>
      <NavigationBar isLogedIn={true} />
      <Container
        className="pt-1 pt-md-2"
        style={{
          marginTop: 56,
        }}
      >
        <h3 className="mt-4 mb-3">Daftar Jual Saya</h3>
        {show && (
          <Alert
            variant="success"
            onClose={() => setShow(false)}
            className="text-white"
            dismissible
          >
            Produk berhasil ditambahkan.
          </Alert>
        )}

        <Card className="mt-2 p-2" style={{ borderRadius: 13 }}>
          <Row>
            <Col xs={8} className="d-flex align-items-center">
              <Card.Img
                style={{ width: 50 }}
                src="https://source.unsplash.com/mWztzk66I7Q/w=600"
                className="me-3"
              />
              <div>
                <p className="m-0" style={{ fontSize: 14 }}>
                  Nama Penjual
                </p>
                <small style={{ fontSize: 10 }}>Kota</small>
              </div>
            </Col>
            <Col className="d-flex align-items-center justify-content-end">
              <Link to="/">
                <button
                  type="button"
                  className="btn btn-outline-secondary"
                  style={{
                    borderRadius: 8,
                  }}
                >
                  Edit
                </button>
              </Link>
            </Col>
          </Row>
        </Card>

        <Row className="mt-3">
          <div className="d-flex d-md-none mb-3 overflow-scroll">
            <button
              className="btn-list-cat active"
              style={{
                paddingRight: "20px",
                paddingLeft: "20px",
              }}
            >
              <p
                className="mx-0 my-auto"
                style={{
                  width: "4rem",
                }}
              >
                <FaSearch /> Semua
              </p>
            </button>
            <button
              className="btn-list-cat active"
              style={{
                paddingRight: "20px",
                paddingLeft: "20px",
              }}
            >
              <p
                className="mx-0 my-auto"
                style={{
                  width: "4rem",
                }}
              >
                <FaSearch /> Hobi
              </p>
            </button>
            <button
              className="btn-list-cat active"
              style={{
                paddingRight: "20px",
                paddingLeft: "20px",
              }}
            >
              <p
                className="mx-0 my-auto"
                style={{
                  width: "4rem",
                }}
              >
                <FaSearch /> Kendaraan
              </p>
            </button>
            <button
              className="btn-list-cat active"
              style={{
                paddingRight: "20px",
                paddingLeft: "20px",
              }}
            >
              <p
                className="mx-0 my-auto"
                style={{
                  width: "4rem",
                }}
              >
                <FaSearch /> Baju
              </p>
            </button>
          </div>
          <Col md={3} className="d-none d-md-block">
            <Card
              style={{ width: "100%", borderRadius: 13, fontSize: "1rem" }}
              className="p-1"
            >
              <div className="p-2 mr-5">
                <Col>Kategori</Col>
              </div>
              <div className="p-2">
                <Col
                  style={{ cursor: "pointer" }}
                  onClick={() => navigate("/form")}
                  className="border-bottom"
                >
                  <FaCube color="#8A8A8A" /> Semua Produk
                  <FaAngleRight color="#8A8A8A" />
                </Col>
              </div>
              <div className="p-2">
                <Col
                  style={{ cursor: "pointer" }}
                  onClick={() => navigate("/seller-daftar-diminati")}
                  className="border-bottom"
                >
                  <FaHeart color="#8A8A8A" /> Diminati
                  <FaAngleRight color="#8A8A8A" />
                </Col>
              </div>
              <div className="p-2">
                <Col
                  style={{ cursor: "pointer" }}
                  onClick={() => navigate("/form")}
                  className="border-bottom"
                >
                  <FaDollarSign color="#8A8A8A" /> Terjual
                  <FaAngleRight color="#8A8A8A" />
                </Col>
              </div>
            </Card>
          </Col>
          <Col md={9}>
            <Row className="row-cols-1 row-cols-2 row-cols-md-3 g-2 g-md-3">
              {cardListProduct()}
              {dummyData.map((data) => cardListProduct(data))}
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default SellerDaftarJual;
