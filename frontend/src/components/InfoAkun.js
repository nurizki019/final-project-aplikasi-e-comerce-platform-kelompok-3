import React from "react";
import { Navbar, Container, Form } from "react-bootstrap";
import { FaChevronLeft } from "react-icons/fa";
import { FiCamera } from "react-icons/fi";
import { useNavigate } from "react-router-dom";

export default function InfoAkun() {

  const infoakun = useNavigate();

  return (
    <div>
      <Navbar className="navs fixed-top shadow" variant="light" bg="light">
        <Container>
          <Navbar.Brand className="text-brand" style={{ color: "#7126B5" }}>SecondHand</Navbar.Brand>
          <div className="text-info text-black">Lengkapi Info Akun</div>
        </Container>
      </Navbar>

      <div className="body-produk">
        <Container>
          <Form className="form-produk box-form">
            <div className="">
              <button onClick={() => infoakun("/")} className="btn-back" href="/">
                <FaChevronLeft />
              </button>
            </div>

            <Form.Group className="foto-akun" controlId="formBasicEmail">
              <div>
                <label for="acc-img">
                  <i className="fi account-img"><FiCamera /></i>
                </label>
                <input type="file" accept="image/*" className="input-file" id="acc-img" />
              </div>
            </Form.Group>

            <Form.Group className="mb-3 myForm" controlId="formBasicEmail">
              <Form.Label className="fname">Nama</Form.Label>
              <Form.Control type="text" placeholder="Nama" />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Kota</Form.Label>
              <Form.Select>
                <option className="text-muted" label="Pilih Kategori" />
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </Form.Select>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Alamat</Form.Label>
              <Form.Control as="textarea" placeholder="Contoh : Jl. Gili Trawangan V" />
            </Form.Group>

            <Form.Group className="mb-3 myForm" controlId="formBasicEmail" onSubmit="return validateForm()" method="post">
              <Form.Label className="fname">No. Handphone</Form.Label>
              <Form.Control type="tel" placeholder="Contoh : +6285238151694" />
            </Form.Group>

            <button className="btn-simpan" variant="primary" type="submit">
              Simpan
            </button>
          </Form>
        </Container>
      </div>
    </div>
  );
}