import React, { useState } from "react";
import "../style/form.css";
import FormSignup from "./FormSignup";
import FormSuccess from "./FormSuccess";
// import Register from "./Register";

const Form = () => {
  const [isSubmitted, setIsSubmitted] = useState(false);

  function submitForm() {
    setIsSubmitted(true);
  }
  return (
    <>
      <div className="form-container">
        <span className="close-btn">×</span>
        <div className="form-content-left d-none d-md-block">
          <div className="form-images" alt="gambar1.png"></div>
          <h1 className="c-putih shand">Second Hand.</h1>
        </div>
        {!isSubmitted ? <FormSignup submitForm={submitForm} /> : <FormSuccess />}
      </div>
    </>
  );
};

export default Form;
