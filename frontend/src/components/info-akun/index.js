import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { Form, Button, Upload, Input, Select } from "antd"
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { AiOutlineCamera } from "react-icons/ai";
import { editProfile, reset, getProfile } from "../../redux/reducers/user"
import './style.css'

const EditAkun = () => {


    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { Option } = Select
    const { TextArea } = Input


    const [imeg, setimeg] = useState()


    const { loading: isLoading, succes: success } = useSelector((state) => state.user.userEdit);
    const { data: user } = useSelector((state) => state.user.userProfile);


    useEffect(() => {
        dispatch(getProfile())
        if (success) {
            dispatch(reset());
            navigate(`/`)
        }
    }, [dispatch, navigate, success])

    const create = async (values) => {
        const value = new FormData()
        value.append('photo', imeg)
        value.append("address", document.getElementById('address').value);
        value.append("phone", document.getElementById('phone').value);
        value.append("user_name", document.getElementById('user_name').value);

        const keys = Object.keys(value);
        let newValues = { [keys[2]]: { [keys[2]]: value[keys[2]] } }
            ;
        console.log(values);
        dispatch(editProfile(value))

    }

    const missCreate = async (values) => {
        dispatch((values))
        console.log(values);
    }

    const handleUpload = (e) => {
        setimeg(e.file)
        if (Array.isArray(e)) {
            return e;
        }
        return e?.fileList;
    };




    return (
        <>

            <Container className="mt">
                <Form
                    onFinish={create}
                    layout="vertical"
                    onFinishFailed={missCreate}
                    autoComplete='off'
                    noValidate
                    initialValues={{
                        'user_name': user?.user_name,
                        'photo': user?.photo,
                        'address': user?.address,
                        'phone': user?.phone,

                    }}>
                    <Form.Item
                        name="image"
                        valuePropName="fileList"
                        getValueFromEvent={handleUpload}

                    >
                        <Upload
                            multiple={false}
                            listType="picture-card"
                            customRequest={null}
                            type='file'
                            name='image'
                            accept='image/*'
                            beforeUpload={(file) => {
                                console.log({ file });
                                return false
                            }}
                            fileList={user?.photo}

                        >

                            <div>
                                <AiOutlineCamera />

                            </div>
                        </Upload>
                    </Form.Item>

                    <Form.Item
                        label="Nama"
                        name="user_name"

                        rules={[
                            {
                                // required: true,
                                message: "Masukan Nama"
                            }
                        ]}
                    >
                        <Input

                            id="user_name"
                            placeholder="Nama"
                        />
                    </Form.Item>


                    <Form.Item
                        label="Kota"
                        name="kota"
                        rules={[
                            {
                                // required: true,
                                message: "Masukan Kota"
                            }
                        ]}
                    >
                        <Select id="kota" value='1' onChange="selectNum()" placeholder="Kota" >
                            <Option value="1">Gorontalo</Option>
                            <Option value="2">Makasar</Option>
                            <Option value="3">Surabaya</Option>
                            <Option value="4">Mataram</Option>
                            <Option value="5">Hanoi</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                        name="address"
                        label="Alamat"
                        rules={[
                            {
                                // required: true,
                                message: "Masukan Alamat"
                            }
                        ]}
                    >
                        <TextArea id="address" rows={4} placeholder="Contoh: Jalan Ikan Hiu 33" />
                    </Form.Item>
                    <Form.Item
                        label="No Handphone*"
                        name="phone"
                        rules={[
                            {

                                message: "Masukan Nomor Hp"
                            }
                        ]}
                    >
                        <Input id="phone" placeholder="contoh :+62 08080808" />
                    </Form.Item>


                    <Form.Item>
                        <Button
                            htmlType="submit"
                            loading={isLoading}
                        >Simpan
                        </Button>
                    </Form.Item>

                </Form>
            </Container>
        </>
    );
};

export default EditAkun;
