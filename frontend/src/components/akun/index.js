import React from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap'
import Penjual from "../../assets/images/Penjual.png";
import jwt_decode from "jwt-decode";
import { getToken } from '../../redux/reducers/user';
import { FiLogOut } from 'react-icons/fi';
import { Navigate } from 'react-router-dom';
import { useNavigate, Link } from 'react-router-dom';

const Akun = () => {

  const navigate = useNavigate()

  // const decoded = jwt_decode(getToken());

  const exitHandler = () => {
    // localStorage.clear()
    navigate('/', { replace: true })
  }
  // console.log(decoded)
  return (
    <div className="mt-2 p-2" style={{ borderRadius: 13 }}>
      <Row>
        <Col>
          {/* <Card.Img style={{ width: 100 }} alt="proses" src={decoded?.user_photo} /> */}
        </Col>
        <Col>
          {/* <p>{decoded?.user_name}</p> */}
        </Col>
      </Row>
      <Row>
        <Link to={"/info-akun"}>
          <Button className="m-1" style={{ fontSize: 14, width: 100 }}>Edit Profile</Button>
        </Link>      </Row>
      <Row>

        <Button className="m-1" style={{ fontSize: 14, width: 100 }}>Pengaturan</Button>
      </Row>
      <Row>

        <Button onClick={() => exitHandler()} style={{ fontSize: 14, width: 100 }}>Keluar</Button>

      </Row>

    </div>
  )
}

export default Akun
