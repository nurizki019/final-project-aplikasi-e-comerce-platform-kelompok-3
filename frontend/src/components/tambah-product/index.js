import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { Form, Button, Upload, Input, Select } from "antd"
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { PlusOutlined } from '@ant-design/icons';
import { postProduct, reset } from "../../redux/reducers/products"
import { toast } from 'react-toastify';
import LoadingBar from "../loading-bar";

import './style.css'

const TambahProduk = () => {

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { Option } = Select
    const { TextArea } = Input

    const [status, setStatus] = useState()

    const [imeg, setimeg] = useState()
    const [value, setValue] = useState()
    const [category, setCategory] = useState()

    const { data: product, loading: isLoading, succes: successCreateProduct } = useSelector((state) => state.products.createProduct);

    useEffect(() => {
        if (successCreateProduct) {
            dispatch(reset());
            navigate('/myProduct', { replace: true })
        }
    }, [dispatch, navigate, successCreateProduct])

    const create = async (values) => {
        const value = new FormData()
        value.append('image', imeg)
        value.append("product_name", document.getElementById('product_name').value);
        value.append("product_price", document.getElementById('product_price').value);
        value.append("category_id", values?.category_id);
        value.append("product_description", document.getElementById('product_description').value);


        const keys = Object.keys(value);

        let newValues = { [keys[2]]: { [keys[2]]: value[keys[2]] } }

        console.log("new value", newValues, values);

        setValue(values)


        console.log(values);
        dispatch(postProduct({ status_id: status, data: value }))

    }

    const missCreate = async (values) => {
        dispatch((values))
        console.log(values);
    }



    const handleUpload = (e) => {
        toast.success("create Berhasil", { theme: "colored" });
        setimeg(e.file)

        if (Array.isArray(e)) {
            return e;
        }

        return e?.fileList;
    };




    return (
        <>

            <Container className="mt">
                {isLoading && <loadingBar />}
                <Form
                    onFinish={create}
                    layout="vertical"
                    onFinishFailed={missCreate}
                    autoComplete='off'
                    noValidate>
                    <Form.Item
                        label="Nama Produk"
                        name="product_name"
                        rules={[
                            {
                                // required: true,
                                message: "Masukan Nama Produk"
                            }
                        ]}
                    >
                        <Input
                            id="product_name"
                            placeholder="Nama Produk"
                        />
                    </Form.Item>

                    <Form.Item
                        label="Harga Produk"
                        name="product_price"
                        rules={[
                            {
                                // required: true,
                                message: "Masukan Harga Produk"
                            }
                        ]}
                    >
                        <Input id="product_price" placeholder="Rp 0,00" />
                    </Form.Item>

                    <Form.Item
                        label="Kategori"
                        name="category_id"
                        rules={[
                            {
                                // required: true,
                                message: "Masukan Kategori"
                            }
                        ]}
                    >
                        <Select id="category_id" onChange="selectNum()" placeholder="Pilih  Kategori" >
                            <Option value="1">Hobi</Option>
                            <Option value="2">Kendaraan</Option>
                            <Option value="3">Baju</Option>
                            <Option value="4">Elektronik</Option>
                            <Option value="5">Kesehatan</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                        name="product_description"
                        label="Deskripsi"
                        rules={[
                            {
                                // required: true,
                                message: "Masukan Deskripsi"
                            }
                        ]}
                    >
                        <TextArea id="product_description" rows={4} placeholder="Contoh: Jalan Ikan Hiu 33" />
                    </Form.Item>

                    <Form.Item
                        label="Foto Produk"
                        name="image"
                        valuePropName="fileList"
                        getValueFromEvent={handleUpload}
                        rules={[
                            {
                                required: true,
                                message: "Masukan Foto"
                            }
                        ]}>
                        <Upload
                            multiple={false}
                            listType="picture-card"
                            customRequest={null}
                            showUploadList={{ showRemoveIcon: true }}
                            type='file'
                            name='image'
                            accept='image/*'
                            beforeUpload={(file) => {
                                console.log({ file });
                                return false
                            }}
                        >

                            <div>
                                <PlusOutlined />
                                <div style={{ marginTop: 8 }}>Upload</div>
                            </div>
                        </Upload>
                    </Form.Item>

                    <Form.Item>
                        <Button
                            onClick={() => setStatus(0)}
                            htmlType="submit"
                            loading={isLoading}
                        > Preview
                        </Button>
                        <Button
                            onClick={() => setStatus(1)}
                            htmlType="submit"
                            loading={isLoading}

                        >Terbitkan
                        </Button>
                    </Form.Item>

                </Form>
            </Container>
        </>
    );
};

export default TambahProduk;
