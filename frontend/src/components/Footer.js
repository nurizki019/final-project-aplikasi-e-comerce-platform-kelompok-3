import React from "react";
import { FaFacebook } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { FaEnvelope } from "react-icons/fa";
import { FaTwitch } from "react-icons/fa";
import { Container } from "react-bootstrap";

const Footer = () => {
  return (
    <div className="footer shadow-lg">
      <Container>
        <div className="foot">
          <div className="social-media mt-3 mb-4">
            <p className="text-dark">Connect with us</p>
            <div className="connect" style={{ marginBottom: "60px" }}>
              <a
                className="fb text-decoration-none text-white"
                href="https://facebook.com"
              >
                <FaFacebook />
              </a>
              <a
                className="fb text-decoration-none text-white"
                href="https://instagram.com/binaracademy"
              >
                <FaInstagram />
              </a>
              <a
                className="fb text-decoration-none text-white"
                href="https://twitter.com"
              >
                <FaTwitter />
              </a>
              <a
                className="fb text-decoration-none text-white"
                href="https://gmail.com"
              >
                <FaEnvelope />
              </a>
              <a
                className="fb text-decoration-none text-white"
                href="https://twitch.com"
              >
                <FaTwitch />
              </a>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Footer;
