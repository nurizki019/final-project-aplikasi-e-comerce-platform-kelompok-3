import React from "react";
import validate from "./ValidateInfo";
import useForm from "./useForm";
import "../style/form.css";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import { Col, Form, FormControl, InputGroup, Row } from "react-bootstrap";
import { FaEye, FaEyeSlash } from "react-icons/fa";

const FormSignup = ({ submitForm }) => {
  const { handleChange, handleSubmit, values, errors } = useForm(submitForm, validate);
  const [ show, setShow ] = useState(false);
  //  1 bikin state type untuk membedakan user menekan tombol daftar atau login
  const [type, setType] = useState();
  const navigate = useNavigate();

  // 10. fungsi ini berjalan jika user sudah pernah login dan mengunjungi halaman login
  // maka akan dilempar ke halaman home
  // caranya dengan mengambil apakah ada token di local storage
  const token = localStorage.getItem('token');
  if (token) {
    navigate('/');
  }

  const location = useLocation();
  const [showButton, setShowButton] = useState(false);
  useEffect(() => {
    setShowButton(location.pathname === "/form");
  }, [location.pathname]);

  const onShowPasswordHandler = () => {
    setShow(!show);
  }

  const styleShowPassword = {
    cursor: 'pointer',
    backgroundColor: '#fff',
    borderRadius: '0 13px 13px 0'
  };

  return (
    <div className="form-content-right w-100">
     {/* 4 ketika di submit, maka handleSubmit dijalankan dan mengirimkan parameter type
      method handleSubmit ada di file useForm */}
      <form onSubmit={(e) => handleSubmit(e, type)} className="form w-100" noValidate>
      <div className="form-inputs-nama">
      {!showButton ? (<h5 className="ms-0">Daftar</h5>):(<h5>Masuk</h5>) }
      </div>
        {!showButton ? (
          <div className="form-inputs-nama">
            <label className="form-label">Nama</label>
            <input
              className="form-input"
              type="nama"
              name="nama"
              placeholder="Masukan nama"
              value={values.nama}
              onChange={handleChange}
            />
            {errors.nama && <p>{errors.nama}</p>}
          </div>
          
        ) : (
          ""
        )}

        <div className="form-inputs">
          <label className="form-label">Email</label>
          <input
            className="form-input"
            type="email"
            name="email"
            placeholder="Contoh: mohamadnurizki427@gmail.com"
            value={values.email}
            onChange={handleChange}
          />
          {errors.email && <p>{errors.email}</p>}
        </div>
        <div className="form-inputs">
          <label className="form-label">Password</label>
          <Row>
            <Col xs={12}>
              <InputGroup>
                <Form.Control
                  className="form-input"
                  type={show ? 'text' : 'password'}
                  name="password"
                  placeholder="Masukan password"
                  value={values.password}
                  onChange={handleChange} />
                <InputGroup.Text style={styleShowPassword} onClick={onShowPasswordHandler}>
                  {show ?
                    <FaEye /> :
                    <FaEyeSlash />
                  }
                </InputGroup.Text>
              </InputGroup>
            </Col>
           </Row>
          {errors.password && <p>{errors.password}</p>}
        </div>
        <div className="form-inputs">
          {!showButton ? (
            // 2 Ketika tombol daftar ditekan, maka state type akan bernilai 'register' atau daftar
            <button onClick={() => setType('register')} className="form-input-btn" type="submit">
              Daftar
            </button>
          ) : (
            // 3 ketika tombol masuk ditekan, maka sate type akan berilai 'login'
            <button onClick={() => setType('login')} className="form-input-btn" type="submit">
              Masuk
            </button>
          )}{" "}
        </div>
          {/* Belum Punya Akun? <Link to="/Register">Daftar di sini</Link>  */}
          {!showButton ? (<div className="form-input-login a">Sudah punya akun? <Link to="/form">Masuk di sini</Link></div>):
          (<div className="form-input-login a">Belum punya akun? <Link to="/Register">Daftar di sini</Link></div>) }
      </form>
    </div>
  );
};

export default FormSignup;
