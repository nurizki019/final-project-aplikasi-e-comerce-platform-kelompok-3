/* eslint-disable jsx-a11y/alt-text */
import React, { useEffect, useState } from "react";
import Carousel from "react-bootstrap/Carousel";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import "./style.css"
import sepatu1 from "../../assets/images/sepatu1.jpg";
import sepatu2 from "../../assets/images/sepatu2.jpg";
import sepatu3 from "../../assets/images/sepatu3.jpg";

const CardBody = (props) => {
  const { data } = props;
  try {
    const parsing = JSON.parse(data?.product_image)

    return (

      <>

        <Carousel className="product-picture__small mt-5">
          <Carousel.Item>
            <img className="d-block w-100" src="{parsing[0]}" />
          </Carousel.Item>
        </Carousel>
        <Col md={6}>
          <Carousel className="product-picture__big">

            <Carousel.Item>
              <img
                className="d-block w-100"
                src={parsing[0]}
                alt="First slide"
                style={{ borderRadius: 13 }}
              />
              <Carousel.Caption>
                {/* <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p> */}
              </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
              <img
                className="d-block w-100"
                src={parsing[1]}
                alt="second slide"
                style={{ borderRadius: 13 }}
              />
              <Carousel.Caption>
                {/* <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p> */}
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={parsing[2]}
                alt="thrid slide"
                style={{ borderRadius: 13 }}
              />
              <Carousel.Caption>
                {/* <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p> */}
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
          <Card className="mt-2 p-2" style={{ borderRadius: 13 }}>
            <Card.Title className="text-black" style={{ fontSize: 14 }}>
              Deskripsi
            </Card.Title>
            <Card.Text style={{ fontSize: 14 }}>
              <div className="lorem">{data?.product_description}</div>
            </Card.Text>
          </Card>
        </Col>
      </>
    )
  } catch (err) {
    console.log("error", err);

  }
}

export default CardBody;



