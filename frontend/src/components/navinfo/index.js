import React from "react";
import { Navbar, Container } from "react-bootstrap";
import { useNavigate, Outlet } from "react-router-dom";
import "./style.css"



const NavInfo = () => {

  const navbar = useNavigate()

  // const { value } = props
  return (
    <>
      <Container>
        <Navbar className="shadow fixed-top mb-3" bg="light" variant="light" >

          <Navbar.Brand onClick={() => navbar("/")} className="text-brand" style={{ color: "#7126B5", cursor: 'pointer' }}>Second Hand</Navbar.Brand>

          <h3></h3>


        </Navbar>

        <Outlet />

      </Container>
    </>
  );
};

export default NavInfo;
