/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { useState, L } from "react";
import { Button } from "antd";
import { RiWhatsappLine } from "react-icons/ri"
import { editOrder } from "../../redux/reducers/orders";
import { useDispatch, useSelector } from "react-redux"
import { useNavigate, Link, useParams, useOutletContext } from "react-router-dom";
import ModalWa from "../../components/modal/modal-wa";
import ModalStatus from "../../components/modal/modal-status";



export default function CardInfoProduk(props) {


    // modal
    const [showModal, setShowModal] = useState(false);
    const [showModalStatus, setShowModalStatus] = useState(false);
    const onBtnClickHandler = () => { setShowModal(true) }
    const onBtnClickHandlerM = () => { setShowModalStatus(true) }
    const [isParentData, setIsParentData] = useState(false);

    const [orders, status, wa] = useOutletContext();

    const [statusId, setStatus] = useState();

    const { id } = useParams()

    const dispatch = useDispatch()
    const navigate = useNavigate()

    const { loading: isLoading, succes: succesUpdate } = useSelector((state) => state.orders.putOrder);
    console.log(succesUpdate, statusId)
    // useEffect(() => {
    //     if (succesUpdate) {
    //         navigate(`../${statusId}/${id}/rejected`);
    //         dispatch(reset())
    //     }
    // }, [dispatch])

    const rejected = (values) => {

        dispatch(editOrder({ id: orders?.id, data: 3 }));
        navigate(`../3/${id}/rejected`, { replace: true });
    }


    const acepted = (values) => {
        setStatus(2)
        dispatch(editOrder({ id: orders?.id, data: 2 }));
        navigate(`../2/${id}/accepted`, { replace: true });
    }

    try {

        const parsing = JSON.parse(orders?.product?.product_image)

        return (
            <>

                <div className="items-offering">
                    <div className="d-flex gap-3 first-div">

                        <img alt="example"
                            src={parsing[0]} />
                        <div>
                            <div
                                className="d-flex"
                                style={{ fontSize: "10px", color: "rgba(138, 138, 138, 1)" }}
                            >
                                <span>Penawaran produk</span>
                                <span className="position-absolute" style={{ right: "25%" }}>
                                    20 Apr, 14.04
                                </span>
                            </div>
                            <h6>{orders?.product?.product_name}</h6>
                            <h6>Rp.{orders?.product?.product_price}</h6>
                            <h6>{orders?.bid}</h6>
                        </div>
                    </div>

                    {/* ----- MODal------- */}
                    {showModal &&
                        <ModalWa
                            isShow={showModal}
                            handleClose={() => setShowModal(false)}
                            data={orders}
                            whatsapp={wa} />
                    }
                    {showModalStatus &&
                        <ModalStatus
                            toChild={isParentData}
                            sendToParent={setIsParentData}
                            sShow={showModalStatus}
                            handleClose={() => setShowModalStatus(false)}
                            data={orders} />
                    }

                    <div className="py-3 d-flex">
                        <div className="ms-auto button-offers">

                            {(orders?.status?.id === 1) &&
                                <>
                                    <button onClick={rejected} className="me-3 py-1" style={{ width: "158px" }}>
                                        Tolak</button>
                                    <button onClick={acepted} className="py-1" style={{ width: "158px" }}
                                    >Terima</button>
                                </>
                            }{orders?.status?.id === 2 &&
                                <>
                                    <Button
                                        onClick={onBtnClickHandlerM}
                                        className="seller-buyer py-1"
                                        style={{ width: "158px" }}>
                                        Status</Button>

                                    <Button
                                        onClick={onBtnClickHandler}
                                        className="seller-buyer py-1"
                                        style={{ width: "158px" }}>
                                        Hubungi Di <RiWhatsappLine /></Button>

                                </>
                            }{orders?.status?.id === 4 &&
                                ""
                            }
                        </div>
                    </div>
                </div>

            </>
        );

    } catch (error) {

    }
}
