import { Card, Row, Col, Image, Text } from "antd";
import React from "react";
import "antd/dist/antd.css";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getProfile } from "../../redux/reducers/user";
import { BsPlusLg } from "react-icons/bs";
import Skeleton from "../skeleton"
import { Container } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import "../../style/SellerDaftarJual.css";
import "./style.css";


const Penjual = (props) => {

    const { user } = props
    console.log(user)

    return (
        <>
            <Container className="border">
                <Row>
                    <Col span={2}>
                        <img
                            height={48}
                            src={user?.photo}
                            alt="contoh"
                        ></img>
                    </Col>
                    <Col span={4}>
                        <span>{user?.user_name}</span>
                        <p>{user?.address}</p>
                    </Col>
                    <Col span={18}>
                        <Link to="/info-akun">
                            <button
                                type="button"
                                className="btn btn-outline-secondary"
                                style={{
                                    right: 10,
                                    borderRadius: 8,
                                    float: "right",
                                    marginTop: 7,
                                    borderRadius: 8,
                                }}
                            >
                                Edit
                            </button>
                        </Link>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default Penjual;
