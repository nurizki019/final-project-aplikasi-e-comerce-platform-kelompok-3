import React, { useEffect, useState } from "react";
import Carousel from "react-bootstrap/Carousel";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import sepatu1 from "../assets/images/sepatu1.jpg";
import sepatu2 from "../assets/images/sepatu2.jpg";
import sepatu3 from "../assets/images/sepatu3.jpg";
import NavigationBar from "./NavigationBar";
import Penjual from "../assets/images/Penjual.png";

import "../style/seller.css"
import BuyerModal from "./BuyerModal";
import { useLocation, useNavigate } from "react-router-dom";
import axios from "axios";

function useQuery() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}

const SellerProduct = ({ isAdmin }) => {
  let query = useQuery()
  const [showModal, setShowModal] = useState(false)
  const navigate = useNavigate();
  const onBtnClickHandler = () => {
    if (!isAdmin) {
      // jika berada di halaman buyyer, maka kode di bawah akan dijalankan
      // saat tombol saya tertarik ditekan, maka state showModal menjadi true (muncul)
      setShowModal(true)
    } else {
      navigate("/seller-jual")
    }
  }

  const [product, setProduct] = useState({
    product_name: 'null',
    product_description: "null",
    product_image: "[\"null\"]",
    product_price: 0,
    category: {
      category_name: 'null'
    },
    user: {
      user_name: 'null',
      photo: "[\"null\"]"
    },
  })

  useEffect(() => {
    const productId = query.get('productId');
    if (productId) {
      axios.get(`https://back-end-seken.herokuapp.com/product/${productId}`)
        .then(res => setProduct(res.data.data))
    }
  }, []);

  const productImages = JSON.parse(product.product_image);

  return (
    <>
      {showModal &&
        // saat state showModal true maka modal akan muncul, dan modal di close property handleCloser
        // dijalankan dan state showModal akan kembali menjadi false (menghilang)
        <BuyerModal isShow={showModal} handleClose={() => setShowModal(false)} />
      }
      <NavigationBar isLogedIn={true} />
      <Carousel className="product-picture__small mt-5">

        {productImages.map((image) => (
          <Carousel.Item>
            <img className="d-block w-100" src={image} alt="First slide" />
            <Carousel.Caption>
              {/* <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p> */}
            </Carousel.Caption>
          </Carousel.Item>
        ))}

      </Carousel>
      <Button
        className="d-block d-md-none seller-btn"
        style={{ background: "#7126b5", borderRadius: 13 }}
        variant="primary w-50"
        onClick={onBtnClickHandler}
      >
        {isAdmin ? "Terbitkan" : "Saya tertarik dan ingin nego "}
      </Button>
      <Container className="px-4 p-md-5 mt-md-5 keterangan-produk">
        <Row className="d-flex flex-column-reverse flex-md-row">
          <Col md={6}>
            <Carousel className="product-picture__big">

              {productImages.map((image) => (
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={image}
                    alt="First slide"
                    style={{ borderRadius: 13 }}
                  />
                  <Carousel.Caption>
                    {/* <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p> */}
                  </Carousel.Caption>
                </Carousel.Item>
              ))}

            </Carousel>
            <Card className="mt-2 p-2" style={{ borderRadius: 13 }}>
              <Card.Title className="text-black" style={{ fontSize: 14 }}>
                Deskripsi
              </Card.Title>
              <Card.Text style={{ fontSize: 14 }}>
                {product.product_description}
              </Card.Text>
            </Card>
          </Col>
          <Col md={6}>
            <Card style={{ borderRadius: 13 }}>
              <Card.Body>
                <Card.Title className="text-black" style={{ fontSize: 16 }}>
                  {" "}
                  {product.product_name}
                </Card.Title>
                <Card.Text className="mb-2 mb-md-3" style={{ fontSize: 14 }}>
                  {product.category.category_name}
                </Card.Text>
                <Card.Text className="mb-0 mb-md-2" style={{ fontSize: 14 }}>
                  {product.product_price}
                </Card.Text>
                <Button
                  className="d-none d-md-block"
                  style={{ background: "#7126b5", borderRadius: 13 }}
                  variant="primary w-100"
                  onClick={onBtnClickHandler}
                >
                  {isAdmin ? "Terbitkan" : "Saya tertarik dan ingin nego "}
                </Button>
                {isAdmin && (
                  <Button
                    onClick={() => navigate("/infoproduk")}
                    className="edit d-none d-md-block"
                    variant="outline-secondary w-100 mt-2"
                    style={{ borderRadius: 13 }}
                  >
                    Edit
                  </Button>
                )}
              </Card.Body>
            </Card>
            <Card className="mt-2 p-2" style={{ borderRadius: 13 }}>
              <Row>
                <Col className="col-2">
                  <Card.Img style={{ width: 50 }} src={Penjual} />
                </Col>
                <Col>
                  <div style={{ fontSize: 14 }}>{product.user.user_name}</div>
                  <small style={{ fontSize: 10 }}>Kota</small>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default SellerProduct;
