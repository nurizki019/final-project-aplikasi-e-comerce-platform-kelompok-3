import React from 'react'
import { Container, Row, Col, Card } from 'react-bootstrap'
import { Link, useLocation, useNavigate } from "react-router-dom";
import NavigationBar from './NavigationBar';
import { Fa500Px, FaCircle, FaCross, FaMaxcdn, FaMix, FaMixer, FaSearch, FaXbox, FaXRay, FaArrowRight, FaPlus, FaHeart, FaDollarSign, FaCube, FaAngleRight} from "react-icons/fa";

const SellerDaftarJualSaya = () => {
    const navigate = useNavigate();
    return (
        <>
          <NavigationBar isLogedIn={true} />
          <Container className='pt-5'>
            <h3 className='my-4'>Daftar Jual Saya</h3>
            <Card className=" mt-2 p-2 " style={{borderRadius:13}}> 
              <Row className="p-1">
                <Col className="col-1 d-flex align-items-center justify-content-center">
                    <Card.Img height={35} src="https://source.unsplash.com/mWztzk66I7Q/w=600" className='me-3'/>
                </Col>
                <Col className="col-9">
                  <div style={{fontSize:14}}>
                    Nama Penjual
                    </div>
                  <small style={{fontSize:10}}>
                    Kota
                  </small>
                </Col>
                <Col>
                <Link to="/infoakun">
                  <button type="button" className="btn btn-outline-secondary" 
                    style={{right: 10, borderRadius:8, float:'right', marginTop:7, borderRadius:8,}}>Edit
                  </button>
                </Link>  
                </Col>
              </Row>
            </Card>

            <Row className='mt-3'>
              <Col className="col-3">
                <Card style={{width: '100%', borderRadius: 13}} className="p-1">
                  <div className="p-2 mr-5">
                  <Col>Kategori</Col>
                  </div>
                  <div className="p-2">
                  <Col style={{cursor: 'pointer'}} onClick={() => navigate('/form')} className='border-bottom'><FaCube color="#8A8A8A"/>    Semuah Produk<FaAngleRight color="#8A8A8A"/>
                  </Col>
                  </div>
                  <div className="p-2">
                  <Col style={{cursor: 'pointer'}} onClick={() => navigate('/form')} className='border-bottom'><FaHeart color="#8A8A8A"/>    Diminati<FaAngleRight color="#8A8A8A"/>
                  </Col>
                  </div>
                  <div className="p-2">
                  <Col style={{cursor: 'pointer'}} onClick={() => navigate('/form')} className='border-bottom'><FaDollarSign color="#8A8A8A"/>    Terjual<FaAngleRight color="#8A8A8A"/>
                  </Col>
                  </div>
                  <div className="p-2">
                  <Col style={{cursor: 'pointer'}} onClick={() => {
                    const confirm = window.confirm('Yakin logout?')
                    if (confirm) {
                      localStorage.clear();
                      navigate('/form');
                    }
                  }} className='border-bottom'><FaArrowRight color="#8A8A8A"/>    Logout<FaAngleRight color="#8A8A8A"/>
                  </Col>
                  </div>
                </Card>
              </Col>
              <Col className="col-9">
                <Row className="d-flex justify-content-start">
                <div className="card card-all col-3 gx-3 ms-3 mb-3" onClick={() => navigate('/infoproduk')}>
                <div className="card-body d-flex align-items-center justify-content-center">
                <Row className="card-body d-flex align-items-center justify-content-center"><FaPlus/>
                  Tambah Produk
                </Row>
              </div>
            </div>
                </Row>
              </Col>
              </Row>
              
                </Container>
            </>
  )
}

export default SellerDaftarJualSaya
