
import { Row, Col, Skeleton } from "antd";
import React from "react";
import { useState } from "react";

const Loading = () => {
    const [active, setActive] = useState(false);
    return (
        <Row gutter={20}  >
            {Array(10)
                .fill()
                .map((el, i) => (
                    <Col span={8} key={i} className="mb-3" >
                        <Skeleton.Image active={setActive}></Skeleton.Image>
                        <Skeleton width={80} heigth={100}></Skeleton>

                    </Col>
                ))
            }
        </Row>
    );
};

export default Loading;