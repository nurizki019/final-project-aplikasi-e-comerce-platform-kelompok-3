import React, { useState } from 'react'
import { Container, Row, Col, Card } from 'antd'

import Group33 from "../../assets/images/Group33.png"
import { type } from '@testing-library/user-event/dist/type'

const Kosong = (props) => {

    const { type } = props

    return (
        <Col className='col-9 justify-content-center d-flex'>
            <div className='text-center'>
                <img src={Group33} />
                <span>
                    <p>Belum ada produkmu yang {type} nih, sabar ya rejeki nggak kemana kok
                    </p>
                </span>

            </div>
        </Col>
    )
}

export default Kosong
