import { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export const TOKEN = localStorage.getItem("token");

const useForm = (callback, validate) => {
  const [values, setValues] = useState({
    username: "",
    email: "",
    password: "",
    password2: "",
  });
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);
  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  // 5. disini yang mengatur dan meng hit API login dan register
  const handleSubmit = (e, type) => {
    e.preventDefault();
    setErrors(validate(values));

    console.log(values);

    // 6 jika type nya adalah login, maka akan menjalankan kode ini
    if (type === "login") {
      console.log("login");
      axios
        .post("https://back-end-seken.herokuapp.com/login", {
          user_name: values.nama,
          email: values.email,
          password: values.password,
        })
        .then((res) => {
          if (res.status === 200) {
            // 7. jika login sukses, maka akan menyimpan token di local storage
            // dan akan di arahkan ke halaman utama
            navigate("/");
            localStorage.setItem("token", res.data.token);
            alert(`Anda berhasil Login`);
          }
        })
        .catch((err) => {
          alert(err.response.data.error);
        });
    }

    // 8 jika type bernilai register maka akan menjalankan kode diawah ini untuk integrasi
    if (type === "register") {
      console.log("register");
      axios
        .post("https://back-end-seken.herokuapp.com/register", {
          user_name: values.nama,
          email: values.email,
          password: values.password,
        })
        .then((res) => {
          if (res.status === 201) {
            // 9 setelah register berhasil, maka user akan diarahkan ke halaman login
            navigate("/form");
            alert("Register berhasil!, silahkan masuk");
          }
        });
    }

    setIsSubmitting(true);
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback();
    }
  }, [errors]);

  return { handleChange, handleSubmit, values, errors };
};

export default useForm;
