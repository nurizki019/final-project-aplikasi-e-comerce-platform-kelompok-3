import React from "react";
import { Card, Row, Col } from "antd";
import { useNavigate } from "react-router-dom";
import { FiBox, FiDollarSign, FiHeart, FiChevronRight } from "react-icons/fi";
import "./style.css";
import "antd/dist/antd.css";

const Category = () => {
    const navigate = useNavigate();
    return (
        <>

            <Card className="p-1 border size">
                <div className="p-2 mr-5">
                    <h4>Kategori</h4>
                </div>
                <div className="p-2 darkB">
                    <Row className="d-flex justify-content-between">
                        <Col
                            style={{ cursor: "pointer" }}
                            onClick={() => navigate("/myProduct")}
                            className="border-bottom"
                        >
                            <FiBox /> Semuah Produk
                        </Col>

                        <Col> <FiChevronRight /></Col>
                    </Row>
                </div>
                <div className="p-2 darkB">
                    <Row className="d-flex justify-content-between">
                        <Col
                            style={{ cursor: "pointer" }}
                            onClick={() => navigate("/wishlist")}
                            className="border-bottom"
                        >
                            <FiHeart /> Diminati
                        </Col>
                        <Col> <FiChevronRight /></Col>
                    </Row>

                </div>
                <div className="p-2 darkB">
                    <Row className="d-flex justify-content-between">
                        <Col
                            style={{ cursor: "pointer" }}
                            onClick={() => navigate("/sold")}
                            className="border-bottom"
                        >
                            <FiDollarSign /> Terjual

                        </Col>
                        <Col> <FiChevronRight /></Col>
                    </Row>

                </div>
            </Card>

        </>)
}

export default Category