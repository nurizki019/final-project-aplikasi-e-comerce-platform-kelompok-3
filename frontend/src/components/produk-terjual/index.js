/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { useEffect } from "react";
import { Card, Row, Col } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getOrderByStatus } from "../../redux/reducers/orders";
import { isEmpty } from "../../utils/empty"
import Skeleton from "../skeleton"
import Kosong from "../../components/kosong"
import "./style.css";

export default function CardListOrdersT() {

    const dispatch = useDispatch();
    const { data: order, loading: isLoading } = useSelector((state) => state?.orders?.status);

    console.log(order);
    const id = 4
    useEffect(() => {
        dispatch(getOrderByStatus(id));
    }, [dispatch, id]);

    try {

        return (
            <>
                <div className="size-s">
                    {isLoading && <Skeleton />}
                    <Row gutter={20} >

                        {(isEmpty(order)) ?
                            <Kosong type='terjual' />
                            :
                            order.map((order, i) => {
                                const parsing = JSON.parse(order.product.product_image);
                                return (
                                    <>
                                        <Col key={i} span={8} className="mb-4">
                                            <Link key={i} to={`/diminati/${order?.status_id}/${order?.id}/${order?.status?.status_name}`}>
                                                <Card key={i}
                                                    className="size border-c p-2"
                                                    bodyStyle={{ padding: "0" }}
                                                    style={{ objectFit: "cover", }}
                                                    cover={
                                                        <img key={i}
                                                            className="size border-g"
                                                            alt="masih dalam proses"
                                                            src={parsing[0]}
                                                        />
                                                    }
                                                >
                                                    <div key={i} className="size-c">
                                                        <p className="title line mt-2">{order.product.product_name}</p>
                                                        <p className="desc line neutral">{order.product.product_description}</p>
                                                        <p className="title">Rp.{order.product.product_price}</p>
                                                    </div>
                                                </Card>
                                            </Link>
                                        </Col>
                                    </>
                                )
                            })
                        }
                    </Row>
                </div>
            </>
        )

    } catch (error) {


    }
}

