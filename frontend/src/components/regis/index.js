import React from "react";
import "./style.css";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import { Form, Button, Input, Row } from "antd";
import { useDispatch, useSelector } from "react-redux"
import { register, reset } from "../../redux/reducers/user";
import { Bars } from "react-loader-spinner"


const FormSignUp = (props) => {

    const { boolean, type, condisi } = props

    const dispatch = useDispatch()
    const navigate = useNavigate();
    const { data, loading, succes: succesRegis } = useSelector((state) => state.user.userRegis);

    const regis = async (values) => {
        dispatch(register(values))
        console.log(values);
    }

    useEffect(() => {
        if (succesRegis) {
            dispatch(reset());
        }
    })


    console.log(data)


    const [form] = Form.useForm();

    const gagalRegis = (errorInfo) => {
        console.log('Failed', errorInfo)
    }

    return (
        <div className="form-content-right w-100">
            {loading && <Bars className="fixed-top" color="#00BFFF" height={80} width={80} />}
            <Form className="form w-100" onFinish={regis} layout="vertical" onFinishFailed={gagalRegis} autoComplete='off' noValidate>
                <div className="left" >
                    <h5>{type}</h5>
                </div>

                <Form.Item
                    className="form-inputs"
                    label="Nama"
                    name="user_name"
                    rules={[
                        {
                            required: true,
                            message: "Masukan Nama"
                        }
                    ]}
                >
                    <Input
                        className="border-i" placeholder="Contoh: Mohamad"
                    />

                </Form.Item>
                <Form.Item
                    className="form-inputs"
                    label="Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: "Masukan Email"
                        }
                    ]}
                >
                    <Input
                        className="border-i" placeholder="Contoh: mohamadnurizki427@gmail.com"
                    />

                </Form.Item>

                <Form.Item
                    className="form-inputs"
                    name="password"
                    label="Password"
                    rules={[
                        {
                            required: true,
                            message: "Masukan Password"
                        }
                    ]}
                >
                    <Input.Password className="border-i" placeholder="Input Password" />
                </Form.Item>

                <div className="form-inputs">
                    <Form.Item>
                        <Button className="form-input-btn"
                            loading={loading}
                            disabled={loading}
                            htmlType="submit">
                            {type}
                        </Button>
                    </Form.Item>
                </div>
                <div className="form-input-login a">{boolean} punya akun? <span style={{ cursor: "pointer" }} onClick={() => navigate("/login")}>{condisi} di sini</span></div>
            </Form>
        </div>

    )
};

export default FormSignUp;
