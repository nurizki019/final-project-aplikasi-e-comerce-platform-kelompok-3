/* eslint-disable react-hooks/rules-of-hooks */
import { useLocation, Navigate, Outlet } from "react-router-dom";
import { useEffect, useState } from "react";
import { getToken } from "../../../redux/reducers/products";

const useAuth = () => {
    const token = localStorage.getItem('token');
    if (token) {
        return true
    }
    else {
        return false
    }

}

const isAuth = () => {

    const auth = useAuth()

    console.log(auth);

    const location = useLocation()
    return (
        auth ? <Outlet />
            :

            <Navigate to="/unauthorized" state={{ from: location }} replace />

    );
}

export default isAuth;