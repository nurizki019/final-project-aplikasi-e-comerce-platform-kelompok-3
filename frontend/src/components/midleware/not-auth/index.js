/* eslint-disable react-hooks/rules-of-hooks */
import { useLocation, Navigate, Outlet } from "react-router-dom";

const useAuth = () => {
    const token = localStorage.getItem('token');
    if (token) {
        return false
    }
    else {
        return true
    }
}

const NotAuth = () => {

    const auth = useAuth()

    console.log(auth);

    const location = useLocation()
    return (
        auth ? <Outlet />
            :
            <Navigate to="/" state={{ from: location }} replace />

    );
}

export default NotAuth;