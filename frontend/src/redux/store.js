import { configureStore } from "@reduxjs/toolkit";
import productSlice from "./reducers/products";
import ordersSlice from "./reducers/orders";
import userSlice from "./reducers/user";

export const store = configureStore({
  reducer: {
    products: productSlice,
    orders: ordersSlice,
    user: userSlice,
  },

  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

