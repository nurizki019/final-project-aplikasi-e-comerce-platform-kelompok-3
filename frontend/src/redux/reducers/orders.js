import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { toast } from 'react-toastify';

export const getToken = () => {
  return localStorage.getItem("token");
}

// Get Order
export const getAllOrders = createAsyncThunk("orders", async () => {
  try {

    const response = await axios.get(
      `http://localhost:8000/orders`,
      {

        headers: { Authorization: `Bearer ${getToken()}` }
      }

    );
    if (response.status === 200) {
      return response
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getOrderWish = createAsyncThunk("orders/wish", async () => {
  try {
    const response = await axios.get(
      `http://localhost:8000/orders/wish`,
      {

        headers: { Authorization: `Bearer ${getToken()}` }
      }

    );
    if (response.status === 200) {
      return response
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getOrderByStatus = createAsyncThunk("orders/status", async (value) => {
  try {


    const response = await axios.get(
      `http://localhost:8000/order/status/${value}`,
      {

        headers: { Authorization: `Bearer ${getToken()}` }
      }

    );
    if (response.status === 200) {
      return response

    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});


export const getStatusById = createAsyncThunk("order/wishlist/id", async (value) => {
  try {

    const id = value?.id;
    const status = value?.status;


    console.log(status)

    const response = await axios.get(
      `http://localhost:8000/order/wishlist/${status}/${id}`,
      {

        headers: { Authorization: `Bearer ${getToken()}` }
      }
    );
    if (response.status === 200) {

      return response
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

// ------------------ Post Order -------------

export const createOrder = createAsyncThunk("orders/create", async (value) => {

  try {
    const statusId = 1;
    const productId = value?.id;
    const data = value?.data;

    const response = await axios.post(
      `http://localhost:8000/orders?status_id=${statusId}&product_id=${productId}`, data,
      {

        headers: { Authorization: `Bearer ${getToken()}` }
      }
    );
    if (response.status === 201) {

      toast.success("Harga tawarmu berhasil dikirim ke penjual");
      return response

    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

// ------------------ Put Order -------------

export const editOrder = createAsyncThunk("edit/order", async (value) => {

  try {

    const id = value?.id
    const data = value?.data

    const response = await axios.put(
      `http://localhost:8000/order/${id}?status_id=${data}`,
      {
        headers: { Authorization: `Bearer ${getToken()}` }
      }
    );

    if (response.status === 200) {

      // toast.success("Berhasil Di update")
      console.log(response);

      return response

    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

const initialState = {
  allOrders: {
    loading: false,
    error: false,
    errorMesseage: false,
    succes: false,
    data: [],
  },

  status: {
    loading: false,
    error: false,
    errorMesseage: false,
    succes: false,
    data: [],
  },

  wishlistId: {
    loading: false,
    error: false,
    errorMesseage: false,
    succes: false,
    data: [],
  },
  wishlist: {
    loading: false,
    error: false,
    errorMesseage: false,
    succes: false,
    data: [],
  },

  postOrder: {
    loading: false,
    error: false,
    succes: false,
    data: []
  },

  putOrder: {
    loading: false,
    error: false,
    succes: false,
    errorMesseage: false,
    data: []
  },


};



export const ordersSlice = createSlice({
  name: "orders",
  initialState,
  reducers: {
    reset: () => initialState,
  },
  extraReducers: {
    //all order
    // builder.addCase(getAllOrders.pending,(state) => {
    //   state.allOrders.loading = true;
    // },
    // ).addCase()
    [getAllOrders.pending]: (state) => {
      state.allOrders.loading = true;
    },
    [getAllOrders.fulfilled]: (state, action) => {
      state.allOrders.data = action.payload.data.data.content;
      state.allOrders.succes = true;
      // state.allorders.error = false;
      state.allOrders.errorMesseage = null;
      state.allOrders.loading = false;
    },
    [getAllOrders.rejected]: (state, action) => {
      state.allOrders.succes = false;
      state.allOrders.error = action.error.Messeage;
      state.allOrders.errorMesseage = action.payload.Messeage;
      state.allOrders.loading = false;
    },

    //by status
    [getOrderByStatus.pending]: (state) => {
      state.status.loading = true;
    },
    [getOrderByStatus.fulfilled]: (state, action) => {
      state.status.data = action.payload.data.data;
      state.status.succes = true;
      state.status.error = false;
      state.status.errorMesseage = null;
      state.status.loading = false;
    },
    [getOrderByStatus.rejected]: (state, action) => {
      state.status.succes = false;
      state.status.error = action.error.Messeage;
      state.status.errorMesseage = action.payload.Messeage;
      state.status.loading = false;
    },
    //wishlish
    [getOrderWish.pending]: (state) => {
      state.wishlist.loading = true;
    },
    [getOrderWish.fulfilled]: (state, action) => {
      state.wishlist.data = action.payload.data.data;
      state.wishlist.succes = true;
      // state.wishlist.error = false;
      state.wishlist.errorMesseage = null;
      state.wishlist.loading = false;
    },
    [getOrderWish.rejected]: (state, action) => {
      state.wishlist.succes = false;
      state.wishlist.error = action.error.Messeage;
      state.wishlist.errorMesseage = action.payload.Messeage;
      state.wishlist.loading = false;
    },

    //wishlishID
    [getStatusById.pending]: (state) => {
      state.wishlistId.loading = true;
    },
    [getStatusById.fulfilled]: (state, action) => {
      state.wishlistId.data = action.payload.data.data;
      state.wishlistId.succes = true;
      state.wishlistId.error = false;
      state.wishlistId.errorMesseage = null;
      state.wishlistId.loading = false;
    },
    [getStatusById.rejected]: (state, action) => {
      state.wishlistId.succes = false;
      state.wishlistId.error = action.error.Messeage;
      state.wishlistId.errorMesseage = action.payload.Messeage;
      state.wishlistId.loading = false;
    },

    // ----------- POST ORDER ---------


    [createOrder.pending]: (state) => {
      state.postOrder.loading = true;
    },
    [createOrder.fulfilled]: (state, action) => {
      state.postOrder.data = action.payload.data;
      state.postOrder.succes = true;
      state.postOrder.error = false;
      // state.postOrder.errorMesseage = action.payload.Messeage;
      state.postOrder.loading = false;
    },
    [createOrder.rejected]: (state, action) => {
      state.postOrder.succes = false;
      state.postOrder.error = action.error.Messeage;
      state.postOrder.loading = false;
    },

    // ----------- PUT ORDER ---------
    [editOrder.pending]: (state) => {
      state.putOrder.loading = true;
    },
    [editOrder.fulfilled]: (state, action) => {
      state.putOrder.data = action.payload.data.message;
      state.putOrder.succes = true;
      state.putOrder.error = false;
      state.putOrder.errorMesseage = action.payload.Messeage;
      state.putOrder.loading = false;
    },
    [editOrder.rejected]: (state, action) => {
      state.putOrder.succes = false;
      state.putOrder.error = action.error.Messeage;
      state.putOrder.loading = false;
    },

  },
});

export const { reset } = ordersSlice.actions;

export default ordersSlice.reducer;
