import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { toast } from 'react-toastify';


export const getToken = () => {
  return localStorage.getItem("token");
}
export const getAllProducts = createAsyncThunk("products", async () => {
  try {
    const response = await axios.get(
      `http://localhost:8000/products`);
    if (response.status === 200) {
      console.log(response);
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getProductsByUser = createAsyncThunk("products/user", async () => {
  try {
    const response = await axios.get(
      `http://localhost:8000/products/user`,
      {
        headers: { Authorization: `Bearer ${getToken()}` }
      }
    )
    if (response.status === 200) {
      console.log(response);
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getProductsByCategory = createAsyncThunk("products/category", async (value) => {
  try {
    const param = value?.category;

    console.log(param)
    const response = await axios.get(
      `http://localhost:8000/products/category?category=[${param}]`,
    )
    if (response.status === 200) {
      console.log(response);
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

export const getProductById = createAsyncThunk("product/id", async (value) => {
  try {
    const response = await axios.get(
      `http://localhost:8000/product/${value}`)

    if (response.status === 200) {
      console.log(response);
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});


//---------------------- POST PRODUCT-----------
export const postProduct = createAsyncThunk("crete/product", async (value) => {
  try {

    const id = value?.status_id
    const data = value?.data

    console.log(id, data);

    const response = await axios.post(
      `http://localhost:8000/products/?status=${id}`, data,
      {

        headers: {
          Authorization: `Bearer ${getToken()}`,
          'content-type': 'multipart/form-data',
        }
      }

    )

    if (response.status === 200) {
      toast.success("Produk berhasil diterbitkan.");
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});


//---------------------- PUT PRODUCT------------

export const putProduct = createAsyncThunk("edit/product", async (value) => {
  try {

    const id = value?.id
    const status_id = value?.status_id
    const data = value?.data

    console.log("rdux", data);

    const response = await axios.put(
      `http://localhost:8000/product/${id}?status_id=${status_id}`, data,
      {

        headers: { Authorization: `Bearer ${getToken()}` }
      }

    )
    if (response.status === 200) {
      toast.success("Produk berhasil diedit.");
      return response;
    }
  } catch (err) {
    if (!err.response) {
      throw err;
    }
  }
});

const initialState = {
  AllProduct: {
    loading: false,
    error: false,
    errorMesseage: false,
    succes: false,
    data: [],
  },
  productByUser: {
    loading: false,
    error: false,
    errorMesseage: false,
    succes: false,
    data: [],
  },
  productByCategory: {
    loading: false,
    error: false,
    errorMesseage: false,
    succes: false,
    data: [],
    image: [],
  },

  productById: {
    loading: false,
    error: false,
    errorMesseage: false,
    succes: false,
    data: [],
  },

  createProduct: {
    loading: false,
    error: false,
    errorMesseage: false,
    succes: false,
    data: [],
  },

  editProduct: {
    loading: false,
    error: false,
    errorMesseage: false,
    succes: false,
    data: [],
  },
};

export const productSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    reset: () => initialState,
  },
  extraReducers: {
    //AllProducts
    [getAllProducts.pending]: (state) => {
      state.AllProduct.loading = true;
    },
    [getAllProducts.fulfilled]: (state, action) => {
      state.AllProduct.data = action.payload.data.data.content;
      state.AllProduct.succes = true;
      state.AllProduct.error = false;
      state.AllProduct.errorMesseage = null;
      state.AllProduct.loading = false;
    },
    [getAllProducts.rejected]: (state, action) => {
      state.AllProduct.succes = false;
      state.AllProduct.error = action.error.Messeage;
      state.AllProduct.errorMesseage = action.payload.Messeage;
      state.AllProduct.loading = false;
    },

    // ----------------BY CATEGORY ------------
    [getProductsByCategory.pending]: (state) => {
      state.productByCategory.loading = true;
    },
    [getProductsByCategory.fulfilled]: (state, action) => {
      state.productByCategory.data = action.payload.data.data.content;
      state.productByCategory.succes = true;
      state.productByCategory.error = false;
      state.productByCategory.errorMesseage = null;
      state.productByCategory.loading = false;
    },
    [getProductsByCategory.rejected]: (state, action) => {
      state.productByCategory.succes = false;
      state.productByCategory.error = action.error.Messeage;
      state.productByCategory.errorMesseage = action.payload.Messeage;
      state.productByCategory.loading = false;
    },

    //getProductsByUser
    [getProductsByUser.pending]: (state) => {
      state.productByUser.loading = true;
    },
    [getProductsByUser.fulfilled]: (state, action) => {
      state.productByUser.data = action.payload.data.data.content;
      state.productByUser.succes = true;
      state.productByUser.error = false;
      state.productByUser.errorMesseage = null;
      state.productByUser.loading = false;
    },
    [getProductsByUser.rejected]: (state, action) => {
      state.productByUser.succes = false;
      state.productByUser.error = action.error.Messeage;
      state.productByUser.errorMesseage = action.payload.Messeage;
      state.productByUser.loading = false;
    },

    //getProductById
    [getProductById.pending]: (state) => {
      state.productById.loading = true;
    },
    [getProductById.fulfilled]: (state, action) => {
      state.productById.data = action.payload.data.data;
      state.productById.succes = true;
      state.productById.error = false;
      state.productById.errorMesseage = null;
      state.productById.loading = false;
    },
    [getProductById.rejected]: (state, action) => {
      state.productById.succes = false;
      state.productById.error = action.error.Messeage;
      state.productById.errorMesseage = action.payload.Messeage;
      state.productById.loading = false;
    },

    // ------------- POST PRODUCT -----------
    [postProduct.pending]: (state) => {
      state.createProduct.loading = true;
    },
    [postProduct.fulfilled]: (state, action) => {
      // state.createProduct.data = action.payload.data.data;
      state.createProduct.succes = true;
      state.createProduct.error = false;
      state.createProduct.errorMesseage = null;
      state.createProduct.loading = false;
    },
    [postProduct.rejected]: (state, action) => {
      state.createProduct.succes = false;
      state.createProduct.error = action.error.Messeage;
      state.createProduct.errorMesseage = action.payload.Messeage;
      state.createProduct.loading = false;
    },

    // ------------- POST PRODUCT -----------
    [putProduct.pending]: (state) => {
      state.editProduct.loading = true;
    },
    [putProduct.fulfilled]: (state, action) => {

      state.editProduct.succes = true;
      state.editProduct.error = false;
      state.editProduct.errorMesseage = null;
      state.editProduct.loading = false;
    },
    [putProduct.rejected]: (state, action) => {
      state.editProduct.succes = false;
      state.editProduct.error = action.error.Messeage;
      state.editProduct.errorMesseage = action.payload.Messeage;
      state.editProduct.loading = false;
    },
  },
});



export const { reset } = productSlice.actions;

export default productSlice.reducer;
