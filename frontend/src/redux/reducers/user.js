import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { toast } from 'react-toastify';
export const getToken = () => {
    return localStorage.getItem("token");
}

export const getProfile = createAsyncThunk("profile", async () => {
    try {
        const response = await axios.get(
            `http://localhost:8000/profile`,
            {
                headers: { Authorization: `Bearer ${getToken()}` }
            }
        );
        if (response.status === 200) {
            console.log(response);
            return response;
        }
    } catch (err) {
        if (!err.response) {
            throw err;
        }
    }
});

export const login = createAsyncThunk("login", async (value) => {
    try {
        const response = await axios.post(
            `http://localhost:8000/login`, value
        );
        console.log(response)
        if (response.status === 200) {
            localStorage.setItem('token', response.data.token)

            return response;
        }
    } catch (err) {
        if (!err.response) {
            throw err;
        }
    }
});

export const register = createAsyncThunk("register", async (value) => {

    try {
        const response = await axios.post(
            `http://localhost:8000/register`, value
        );
        if (response.status === 201) {
            console.log(response);
            toast.success("Berhasil Mendaftar");
            return response

        }
    } catch (err) {
        if (!err.response) {
            throw err;
        }
    }
});

export const editProfile = createAsyncThunk("profile/edit", async (value) => {
    try {
        const response = await axios.put(
            `http://localhost:8000/profile`, value,
            {
                headers: { Authorization: `Bearer ${getToken()}` }
            }
        );
        if (response.status === 200) {
            console.log(response);
            toast.success("Berhasil Mengubah data");
            return response;
        }
    } catch (err) {
        if (!err.response) {
            throw err;
        }
    }
});


const initialState = {
    userProfile: {
        loading: false,
        error: false,
        errorMesseage: false,
        succes: false,
        data: [],
    },
    userLogin: {
        loading: false,
        error: false,
        errorMesseage: false,
        succes: getToken() ? true : false,
        token: getToken()
    },

    userRegis: {
        loading: false,
        error: false,
        succes: false,
        data: []
    },

    userEdit: {
        loading: false,
        error: false,
        succes: false,
        data: []
    },
};



export const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        reset: () => initialState,
    },
    extraReducers: {
        // userProfile
        [getProfile.pending]: (state) => {
            state.userProfile.loading = true;
        },
        [getProfile.fulfilled]: (state, action) => {
            state.userProfile.data = action.payload.data;
            state.userProfile.succes = true;
            state.userProfile.error = false;
            state.userProfile.errorMesseage = null;
            state.userProfile.loading = false;
        },
        [getProfile.rejected]: (state, action) => {
            state.userProfile.succes = false;
            state.userProfile.error = action.error.Messeage;
            state.userProfile.errorMesseage = action.payload.Messeage;
            state.userProfile.loading = false;
        },

        //userLogin
        [login.pending]: (state) => {
            state.userLogin.loading = true;
        },
        [login.fulfilled]: (state, action) => {
            state.userLogin.token = action.payload.data.token;
            state.userLogin.succes = true;
            state.userLogin.error = false;
            state.userLogin.errorMesseage = null;
            state.userLogin.loading = false;
        },
        [login.rejected]: (state, action) => {
            state.userLogin.succes = false;
            state.userLogin.error = action.error.Messeage;
            state.userLogin.errorMesseage = action.payload.Messeage;
            state.userLogin.loading = false;
        },

        //userRegistr
        [register.pending]: (state) => {
            state.userRegis.loading = true;
        },
        [register.fulfilled]: (state, action) => {
            state.userRegis.data = action.payload.data.message;
            state.userRegis.succes = true;
            state.userRegis.error = false;
            state.userLogin.errorMesseage = action.payload.Messeage;
            state.userRegis.loading = false;
        },
        [register.rejected]: (state, action) => {
            state.userRegis.succes = false;
            state.userRegis.error = action.error.Messeage;
            state.userRegis.loading = false;
        },

        //edit
        [editProfile.pending]: (state) => {
            state.userEdit.loading = true;
        },
        [editProfile.fulfilled]: (state, action) => {
            state.userEdit.succes = true;
            state.userEdit.error = false;
            state.userEdit.errorMesseage = null;
            state.userEdit.loading = false;
        },
        [editProfile.rejected]: (state, action) => {
            state.userEdit.succes = false;
            state.userEdit.error = action.error.Messeage;
            state.userEdit.errorMesseage = action.payload.Messeage;
            state.userEdit.loading = false;
        },
    },
});

export const { reset } = userSlice.actions;

export default userSlice.reducer;
