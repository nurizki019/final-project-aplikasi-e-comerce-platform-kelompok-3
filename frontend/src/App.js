import "./App.css";
import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import reportWebVitals from "./reportWebVitals";

import "bootstrap/dist/css/bootstrap.min.css"; //ambil dari web react-bootstrap
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import "./index.css";

import NavigationBar from "./components/navbar";
import NavInfo from "./components/navinfo";
import EditProduk from "./components/edit-product";
import TambahProduk from "./components/tambah-product";
import InfoAkun from "./components/info-akun";

import CardInfoProduk from "./components/card-info-product";
import CardListProduk from "./components/produk-saya";
import CardListOrdersM from "./components/produk-diminati";
import CardListOrdersT from "./components/produk-terjual";
import Layout from "./components/layout"


import Homepage from "./Pages/Hompage"
import DaftarJual from "./Pages/daftarJual";
import Auth from "./Pages/auth"
import SellerProduct from "./Pages/buyer-product";
import BuyerProduct from "./Pages/suyer-product";
import SellerInfoProduk from "./Pages/seller-info-product";

import IsAuth from "./components/midleware/is-auth";
import Unauthorized from "./utils/Unauthorized";
// import Notif from "./components/SendNotif";
import EditAkun from "./components/info-akun";


const user = false

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>

            {/* public */}
            <Route path="unauthorized" element={<Unauthorized />} />
            <Route path="login" element={<Auth />} />
            <Route path="register" element={<Auth />} />
            {/* <Route path="notif" element={<Notif />} /> */}

            <Route element={<NavigationBar />}>
              <Route path="/" element={<Homepage />} />
              <Route path="seller-product/:id" element={<SellerProduct />} />
            </Route>

            {/* privat */}
            <Route element={<IsAuth />}>
              <Route element={<NavigationBar />}>
                <Route element={<DaftarJual />}>
                  <Route path="/myProduct" element={<CardListProduk />} />
                  <Route path="/wishlist" element={<CardListOrdersM />} />
                  <Route path="/sold" element={<CardListOrdersT />} />
                </Route>
                <Route path="/seller-product/:id" element={<SellerProduct />} />
                <Route path="/buyer-product/:id" element={<SellerProduct />} />
                <Route path="/" element={<Homepage />} />
              </Route>

              <Route element={<NavInfo />}>
                <Route path="/edit-produk/:id" element={<EditProduk />} />
                <Route path="/tambah-produk" element={<TambahProduk />} />
                <Route path="/info-akun" element={<EditAkun />} />
                <Route path="/diminati/" element={<SellerInfoProduk />} >
                  <Route path=":status/:id/wishlist" element={<CardInfoProduk />} />
                  <Route path=":status/:id/accepted" element={<CardInfoProduk />} />
                  <Route path=":status/:id/rejected" element={<CardInfoProduk />} />
                  <Route path=":status/:id/sold" element={<CardInfoProduk />} />
                </Route>
              </Route>
            </Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </div >
  );
}
reportWebVitals()
export default App;
