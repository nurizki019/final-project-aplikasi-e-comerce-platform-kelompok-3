import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getProductById } from "../../redux/reducers/products";
import "./style.css"
import CardBody from "../../components/card-body";
import CarouselD from "../../components/carousel";

import { Container, Row } from "react-bootstrap";

const BuyerProduct = () => {

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams()


  const { data: productId } = useSelector((state) => state?.products?.productById);
  console.log(productId)

  useEffect(() => {
    dispatch(getProductById(id));
  }, [dispatch, id]);
  return (

    <Container className="px-4 p-md-5 mt-md-5 keterangan-produk">
      <Row className="d-flex flex-column-reverse flex-md-row">
        <CardBody data={productId} />
        <CarouselD data={productId} />

      </Row>
    </Container>
  );
};

export default BuyerProduct;
