import React, { useState } from "react";
import { Container, Navbar } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { Outlet, useParams } from "react-router-dom";
import { getStatusById } from "../../redux/reducers/orders";
import Penjual from "../../components/penjual";
import CardInfoProduk from "../../components/card-info-product";

import "./style.css";
export default function SellerInfoProduk() {

    const { id, status } = useParams()


    const dispatch = useDispatch();
    const { data: orders } = useSelector((state) => state?.orders?.wishlistId);

    const [wa, setWa] = useState()


    console.log(orders)

    useEffect(() => {
        setWa(orders?.user)
        dispatch(getStatusById({ id: id, status: status }));
    }, [dispatch, id, status]);
    console.log("wa", wa)
    return (
        <>
            {status === 3 ?

                (<h1>PRODUK dITOLAK</h1>
                ) :

                (
                    <>

                        <Container className="offers-page">
                            <div className="product-offering">
                                <div className="d-flex gap-3 align-items-center p-3 mb-4">
                                    <Penjual user={orders?.user} />
                                </div>
                                <div className="items-offering">
                                    {status === 4 ?
                                        <h6 className="mb-4">Daftar Produkmu Yang Ditawar</h6>
                                        :
                                        <h6 className="mb-4">Daftar Produkmu Yang Ditawar</h6>
                                    }
                                    <Outlet context={[orders, status, wa]}></Outlet>


                                </div>
                            </div>
                        </Container></>
                )
            }

        </>
    );
}
