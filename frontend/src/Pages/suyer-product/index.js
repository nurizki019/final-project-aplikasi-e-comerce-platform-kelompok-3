import React, { useEffect, useState } from "react";
import "./style.css"
import { useLocation, useNavigate } from "react-router-dom";
import axios from "axios";
import CardBody from "../../components/card-body";
import CarouselD from "../../components/carousel";
import { Container, Row } from "react-bootstrap";
import products from "../../redux/reducers/products";

const SellerProduct = () => {

  return (

    <Container className="px-4 p-md-5 mt-md-5 keterangan-produk">
      <Row className="d-flex flex-column-reverse flex-md-row">
        <CardBody />
        <CarouselD />
        {/* <NavigatorBar/> */}
      </Row>
    </Container>
  );
};

export default SellerProduct;
