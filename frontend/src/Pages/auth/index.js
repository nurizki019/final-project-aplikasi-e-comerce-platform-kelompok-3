import React, { useState, useEffect } from "react";
import "./style.css";
import Login from "../../components/login";
import Register from "../../components/regis";
import { Link, useLocation, useNavigate } from "react-router-dom";
import FormSuccess from "../../components/succesLogin";

const Form = () => {

    const location = useLocation();
    const [login, setLogin] = useState(false);
    const [register, setRegister] = useState(false);
    useEffect(() => {
        setLogin(location.pathname === "/login");
        setRegister(location.pathname === "/register");
    }, [location.pathname]);


    return (
        <>
            <div className="form-container">
                <span className="close-btn">×</span>
                <div className="form-content-left d-none d-md-block">
                    <div className="form-images" alt="login.png"></div>
                    <h1 className="c-putih shand">Second Hand.</h1>
                </div>
                {login && <Login type='Masuk' boolean='Belum' condisi='Daftar' />}
                {register && <Register type='Daftar' boolean='Sudah' condisi='Masuk' />}
            </div>
        </>
    );
};

export default Form;
