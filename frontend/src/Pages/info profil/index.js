import { Navbar, Container, Form } from "react-bootstrap";
import { FaChevronLeft } from "react-icons/fa";
import { FiCamera } from "react-icons/fi";
import { Link, useNavigate } from "react-router-dom";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getUser } from "../../redux/reducers/user";

export default function InfoAkun() {

    const dispatch = useDispatch();
    const { data: user } = useSelector((state) => state?.user?.userLogin);
    const { loading: isLoading } = useSelector((state) => state?.user?.userLogin);

    const navigate = useNavigate();
    console.log(user);

    useEffect(() => {
        dispatch(getUser());
    }, [dispatch]);

    const infoproduk = useNavigate();
    return (
        <div>
            <Navbar className="navs fixed-top shadow" variant="light" bg="light">
                <Container>
                    <Navbar.Brand className="text-brand">SecondHand</Navbar.Brand>
                </Container>
            </Navbar>

            <div className="body-produk">
                <Container>
                    <Form className="form-produk box-form">
                        <div className="">
                            <button onClick={() => infoproduk("/")} className="btn-back" href="/">
                                <FaChevronLeft />
                            </button>
                        </div>

                        <Form.Group className="foto-akun" controlId="formBasicEmail">
                            <div>
                                <label for="acc-img">
                                    <i className="fi account-img"><FiCamera /></i>
                                </label>
                                <input type="file" className="input-file" id="acc-img" />
                            </div>
                        </Form.Group>

                        <Form.Group className="mb-3 myForm" controlId="formBasicEmail">
                            <Form.Label className="fname">Nama</Form.Label>
                            <Form.Control type="text" placeholder="Nama" />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Kota</Form.Label>
                            <Form.Select>
                                <option className="text-muted" label="Pilih Kategori" />
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Alamat</Form.Label>
                            <Form.Control as="textarea" placeholder="Contoh : Jl. Gili Trawangan V" />
                        </Form.Group>

                        <Form.Group className="mb-3 myForm" controlId="formBasicEmail" onSubmit="return validateForm()" method="post">
                            <Form.Label className="fname">No. Handphone</Form.Label>
                            <Form.Control type="tel" placeholder="Contoh : +6285238151694" />
                        </Form.Group>

                        <button className="btn-simpan" variant="primary" type="submit">
                            Simpan
                        </button>
                    </Form>
                </Container>
            </div>
        </div>
    );
}
