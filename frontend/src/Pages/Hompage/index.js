import React from "react";
import { useEffect, useState } from "react";
import { Col, Row, Container, Button } from "react-bootstrap";
import { FaSearch } from "react-icons/fa";
import { TbPlus } from "react-icons/tb";
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom";
import OwlCarousel from "react-owl-carousel";
import { useNavigate } from "react-router-dom";
import { getProductsByCategory } from "../../redux/reducers/products";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

import gambar1 from "../../assets/images/ramadhan.jpg"
import gambar2 from "../../assets/images/merdeka.jpg"
import gambar from "../../assets/images/newyear.jpg"

import "./style.css"

const Homepage = () => {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { data: products, loading: isLoading } = useSelector((state) => state.products.productByCategory);

  const [category, setCategory] = useState([1, 2, 3, 4, 5])
  const [active, setActive] = useState(true);


  console.log(active)
  const categoryHandler = (value) => {

    setActive(false)

    if (value === 'semua') return setCategory([1, 2, 3, 4, 5])
    else if (value === 'hobi') return setCategory([1])
    else if (value === 'kendaraan') return setCategory([2])
    else if (value === 'baju') return setCategory([3])
    else if (value === 'elektronik') return setCategory([4])
    else if (value === 'kesehatan') return setCategory([5])
  }
  useEffect(() => {
    dispatch(getProductsByCategory({ category: category }));
  }, [category, dispatch]);

  return (
    <Container>

      {/* <Carousel
      autoPlay
      centerMode
      centerSlidePercentage={65}
      emulateTouch
      infiniteLoop
      selectedItem={1}
      showArrows={false}
      showStatus={false}
      showThumbs={false}
      showIndicators={false}
      className="mt-5 pt-4 pb-4"
    >
      {data.map(({ img }) => (
        <div key={uuid()} className={[styles.box]}>
          <img className="px-2" src={img} alt={img} layout="fill" />
        </div>
      ))}
    </Carousel> */}


      {/* <section className="testimonial mb-4" id="testimonial">
            <OwlCarousel
              autoplay
              centerMode
              emulateTouch
              infiniteLoop
              selectedItem={1}
              showArrows={false}
              showStatus={false}
              showThumbs={false}
              showIndicators={false}
              dots={false}
              items={2}
              className="owl-theme"
              loop
              center
              margin={10}
            >

              <Row className="mt-4">
                <Col>
                  <div className="item-ramadhan">
                    <div className="testi-body">
                      <div className="card-right">
                        <h1 className="card-title">Bulan Ramadhan</h1>
                        <h1 className="card-title">Bulannya Diskon!</h1>
                        <h6 className="card-title">Diskon Hingga</h6>
                        <p className="diskon">70%</p>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col>
                  <div className="item-merdeka">
                    <div className="testi-body">
                      <div className="card-left">
                        <h1 className="card-title">Bayar Setengah</h1>
                        <h1 className="card-title">di Hari Merdeka!</h1>
                        <p className="card-title">Diskon Hingga</p>
                        <p className="diskon">50%</p>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col>
                  <div className="item-newyear">
                    <div className="testi-body">
                      <div className="card-left">
                        <h1 className="card-title text-white">Tahun Baru</h1>
                        <h1 className="card-title">Barang Baru Ga Si'</h1>
                        <h6 className="card-title">Diskon Hingga</h6>
                        <p className="diskon">35%</p>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </OwlCarousel>
          </section> */}


      {/* mahmud */}
      <Container>
        <OwlCarousel
          className="owl-theme"
          loop margin={10}
          nav
          autoPlay
          centerMode
          emulateTouch
          infiniteLoop
          selectedItem={1}
          showArrows={true}>
          <div class="item">
            <img src={gambar1} />
          </div>
          <div class="item">
            <img src={gambar2} />
          </div>
          <div class="item">
            <img src={gambar} />
          </div>
          <div class="item">
            <img src={gambar1} />
          </div>
          <div class="item">
            <img src={gambar2} />
          </div>
          <div class="item">
            <img src={gambar} />
          </div>
        </OwlCarousel>
      </Container>

      <Container>
        <div className="search-title">
          <h1>Telusuri Kategori</h1>{" "}
        </div>{" "}
        <div className="btn-category">
          {" "}
          <div className="">
            {" "}
            <Button onClick={() => categoryHandler('semua')} className="btn-list-cat" loading={isLoading} active={active}>
              <FaSearch /> Semua{" "}
            </Button>{" "}
            <Button onClick={() => categoryHandler('hobi')} className="btn-list-cat" loading={isLoading}><FaSearch /> Hobi{" "}
            </Button>{" "}
            <Button onClick={() => categoryHandler('kendaraan')} className="btn-list-cat" loading={isLoading}><FaSearch /> Kendaraan{" "}
            </Button>{" "}
            <Button onClick={() => categoryHandler('baju')} className="btn-list-cat" loading={isLoading}><FaSearch /> Baju{" "}
            </Button>{" "}
            <Button onClick={() => categoryHandler('elektronik')} className="btn-list-cat" loading={isLoading}><FaSearch /> Elektronik{" "}
            </Button>{" "}
            <Button onClick={() => categoryHandler('kesehatan')} className="btn-list-cat" loading={isLoading}><FaSearch /> Kesehatan{" "}
            </Button>{" "}
          </div>{" "}
        </div>
        <div className="d-flex">
          <Row>
            {
              products.map(product => {
                try {
                  const parsing = JSON.parse(product?.product_image)



                  return (
                    <Col className="d-flex justify-content-start mt-4">
                      <Link to={`/buyer-product/${product?.id}`}>
                        <div className="card card-all">
                          <div className="card-body">
                            <img src={parsing[0]} alt="maintenance" />
                            <h4 className="text-merk text-dark mt-2">{product?.product_name}</h4>
                            <p className="card-description text-muted">{product?.product_description}</p>
                            <h5 className="text-merk text-dark mt-2">{product?.product_price}</h5>
                          </div>
                        </div>
                      </Link>
                    </Col>


                  )
                } catch (err) {
                  console.log("error", err);

                }
              }
              )}
          </Row>
        </div>
        <Button loading={isLoading} onClick={() => navigate("/tambah-produk")} className="btn-jual fixed-bottom">
          <TbPlus /> Jual
        </Button>
      </Container>
    </Container >
  );
};

export default Homepage;
