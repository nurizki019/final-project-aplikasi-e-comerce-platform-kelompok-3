/* eslint-disable no-unused-vars */
import React from "react";
import { Container } from "react-bootstrap";
import { Row, Col } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useLocation, Outlet, } from "react-router-dom";
import { getProfile } from "../../redux/reducers/user";
import NavigationBar from "../../components/navbar";
import Penjual from "../../components/penjual";
import Category from "../../components/categori";
import CardListProduct from "../../components/produk-saya"
import CardListOrdersM from "../../components/produk-diminati"
import CardListOrdersT from "../../components/produk-terjual"
import "./style.css"
import { useState, useEffect } from "react";

const SellerDaftarJualSaya = () => {

  const location = useLocation();
  const dispatch = useDispatch();

  const { data: user, loading: isLoading } = useSelector((state) => state.user.userProfile);

  useEffect(() => {
    dispatch(getProfile());
  }, [dispatch]);



  return (
    <>
      <Container className="mt-5 ">
        <Row>
          <h3 className="my-4">Daftar Jual Saya</h3>
          <Penjual user={user} />
        </Row>
        <Row gutter={20} className="mt-3 ">
          <Col className="col-3">
            <Category className=" me-3" />
          </Col>
          <Col className="col-9">
            <Row className="d-flex justify-content-start">
              <Outlet />
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default SellerDaftarJualSaya;
