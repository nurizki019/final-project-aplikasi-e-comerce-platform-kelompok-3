export function isEmpty(orders) {
    return Object.keys(orders).length === 0;
}